Compile with: 
mpic++ task2.cpp -std=c++17

add -DDEBUG flag for additional output and ordered printouts


Run with: 
mpirun -np 17 ./a.out < input.txt 2> log.txt


For task2 we decided to use a normal round robin that does not consider the queue sizes to each link. 


Implementation 2 takes on average 3-4 times as long time as task . The reasons for this is that we focused our implemented on a working, not speed.
Task2 has a master and slave communication type, e.g. all slaves send their updates to the master process. We think that the master process need to compute more instructions 
than the average slave, therefore the workload is uneven. 

_________________TASK_1____________________________________________________
 Performance counter stats for './task1':

       2256,473472      task-clock (msec)         #    2,145 CPUs utilized          
           403 964      context-switches          #    0,179 M/sec                  
           118 984      cpu-migrations            #    0,053 M/sec                  
               339      page-faults               #    0,150 K/sec                  
     5 719 190 851      cycles                    #    2,535 GHz                    
   <not supported>      stalled-cycles-frontend  
   <not supported>      stalled-cycles-backend   
     3 557 395 350      instructions              #    0,62  insns per cycle        
       732 220 035      branches                  #  324,498 M/sec                  
         8 975 771      branch-misses             #    1,23% of all branches        

       1,052172054 seconds time elapsed

________________TASK_2________________________________________________________
 Performance counter stats for 'mpirun -np 17 ./task2':

      28785,843239      task-clock (msec)         #    7,221 CPUs utilized          
        16 339 378      context-switches          #    0,568 M/sec                  
             1 149      cpu-migrations            #    0,040 K/sec                  
            29 352      page-faults               #    0,001 M/sec                  
    83 045 931 190      cycles                    #    2,885 GHz                    
   <not supported>      stalled-cycles-frontend  
   <not supported>      stalled-cycles-backend   
    51 309 896 311      instructions              #    0,62  insns per cycle        
    10 206 888 887      branches                  #  354,580 M/sec                  
        28 598 374      branch-misses             #    0,28% of all branches        

       3,986445663 seconds time elapsed



At N= 10 000
green: 3 trains -> 17.139336, 17.139336, 17.139336
yellow: 3 trains -> 25.408318, 25.408318, 25.408318
blue: 3 trains -> 18.426527, 18.426527, 18.426527


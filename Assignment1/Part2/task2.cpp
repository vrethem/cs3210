/* Problem with first implementation: 
    - Train should start each second from both end station until all trains are released.
    - Only one train w/ open doors at station 
*/
#include <mpi.h>
#include <stdio.h>
#include <deque>
#include <vector>
#include <utility>
#include "parser.h"
#include <assert.h>     /* assert */
#include <sstream>
#include <iomanip>
#include <iterator>

#define MASTER 0
#define OFFSET 100000
#define INF 998
using namespace std;

//#define DEBUG // can be defined at compile-time by -DDEBUG flag
#ifdef DEBUG
#define IF_DEBUG(...) __VA_ARGS__
#else
#define IF_DEBUG(...)
#endif
string eEnum[8] = { "INIT", "SEND", "WAIT", "WAIT_SLOT", "OPEN_DOORS", "CLOSE_DOORS", "MOVING", "ARRIVED"};
enum Enum { INIT=0, SEND =1, WAIT=2, WAIT_SLOT=3, OPEN_DOORS=4, CLOSE_DOORS=5, MOVING=6, ARRIVED=7 };

class Li
{
    public:
    Li(int l, int i, Enum s) { line = l, index = i; status=s; }
    int line;
    int index;
    int i_tot =0;
    int send_time;
    int from;
    int to;
    int status;
    bool print = false;
};
class Train
{
  public:
  Train(int l, int tot, int i, Enum s, bool print){ id = l, i_tot=tot; w_time = i; status = s; send =print;}
    int id;
    int i_tot;
    int w_time;
    int status;
    bool send;
};

void slave_stop_clock(int tic, std::deque<Train>::reverse_iterator it);
void slave_start_clock(int tic, std::deque<Train>::reverse_iterator it);

// Empty vectors, let master allocate
// <Master varables>
vector<Li> t_table;
int S, N;
vector<string> L;
char LINE[ROUTES] = {'g', 'y', 'b'};
int T_trains[ROUTES];
vector<float> P;
string Lines[ROUTES] = {"green", "yellow", "blue"};
vector<vector<int> > R(ROUTES);
vector<vector<int> > M;
vector<vector<int> > LINKS(ROUTES);
// </Master vaiables>

// <Slave variables>
int pid;
int world_size;
int link_cost;
float popularity;
int dest;
int source;
vector<deque<Train> > queue(3);
bool first_run = true;
int selector;
int slot_selector;

float w_avr_count[ROUTES] = {0, 0, 0};
float w_avr_sum[ROUTES] = {-1, -1, -1};
float w_longest[ROUTES] = {-1, -1, -1}; // Each link can have up to three different wait times (for each MRT-line)
float w_shortest[ROUTES] = {999, 999, 999};
int myclock[ROUTES] = {0,0,0};
// </Slave variables>


int find_link(int from_, int to_) 
{
    int link_id = 1;
    for (int from = 0; from < S; from++)
    {
        for (int to = 0; to < S; to++)
        {
            if (from == from_ && to == to_)
                return link_id;
            if (M[from][to] > 0)
                link_id++;
        }
    }
    exit(-1);
}

int get_t_line(int id)
{
  //  fprintf(stderr, "[0] == %d [1] == %d [2] == %d \n",T_trains[0], T_trains[1], T_trains[2]);
    int tmp = id;
    for (int route =0; route < ROUTES; route++)
    {
        if (tmp < T_trains[route])
            return route;
        tmp -= T_trains[route];
    }
    fprintf(stdout, "<<ERROR in get_t_line func, para id == %2d, T_trains[%d] == %d >>\n",id,0,T_trains[0]);
    return ROUTES-1;
}

int get_local_tid(int id)
{
    int ret  = id;
    for (int route =0; route < ROUTES; route++)
    {
        if (ret >= T_trains[route])
            ret -= T_trains[route];
        else 
            break;
    }
    return ret;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////  MASTER FUNC   ////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Parse input file and allocate memory for it
*/
void master_parse_input()
{
    read_input(S,
               N,
               L,
               R,
               T_trains,
               P,
               M);
    IF_DEBUG(
        test_input(S,
                   N,
                   L,
                   R,
                   T_trains,
                   P,
                   M);)

    // Create a list of links for each line
    for (int line = 0; line < ROUTES; line++)
    {
        // Walkthrue all subgraph's and assign links
        vector<int> sg = R.at(line);
        for (int i = 0; i < sg.size() -1; i++) 
        {
            int link_id = find_link(sg.at(i), sg.at(i+1));
            LINKS[line].push_back(link_id);
        }
        for (int i = sg.size()-1; i > 0; i--) 
        {
            int link_index = find_link(sg.at(i), sg.at(i-1));
            LINKS[line].push_back(link_index);
        }
        IF_DEBUG(
            fprintf(stdout, "+++ Master :Line %2d with subgraph: ", line);
            for (int i = 0; i < LINKS.at(line).size(); i++)
                fprintf(stdout, "%2d, ", LINKS[line].at(i));
            fprintf(stdout, "\n");)
    }

    // Create train table: t_table 
    // Loop for T_line's
    for (int line = 0; line < ROUTES; line++)
    {
        // Loop for trains on currect t_line, insert to t_table
        for (int i = 0; i < T_trains[line]; i++)
        {
            Li item(line, 0, INIT);
            int index;
            if ( i < T_trains[line]/2)
            {
                item.index = 0;
                item.send_time = i;
            }
            else 
            {
                item.index = LINKS.at(line).size() / 2;
                item.send_time = i - T_trains[line]/2;
            }

            
            //fprintf(stderr, "item->send_status == %d \n", item.send_time);
            if (item.send_time == 0)
                item.status = SEND;

            t_table.push_back(item);
        }
    }

    IF_DEBUG(
        fprintf(stdout, "+++ Master :__________________________\n");
        fprintf(stdout, "+++ Master : |_row_|_Line_|_InDx_|Status|\n");
        for (int i = 0; i < t_table.size(); i++)
            fprintf(stdout, "+++ Master :%4d: | %3d  | %3d  | %3d  |\n", i, t_table.at(i).line, t_table.at(i).index, (int)t_table.at(i).status); //cout << i << t_table[i].print() << endl;
    )

    int slaves = 1;
    for (int from = 0; from < S; from++)
        for (int to = 0; to < S; to++)
            if (M[from][to] > 0)
                slaves++;
    assert(slaves == world_size && "# slaves must be one less than world_size e.g. -np 17");
}

/*
* Initialize slaves to have right link cost and popularity
*/
void master_init()
{
    for (int slave_id = 1; slave_id < world_size; slave_id++)
    {
        // Find link cost and popularity 
        int head = 0;
        int link_id = 0;
        int from, to;
        bool inserted = false;
        for (from = 0; link_id != slave_id; ++from)
        {
            for (to = 0; to < S; to++)
            {
                if (from == to) continue;
                if (M[from][to] > 0)
                {
                    link_id++;
                    if (link_id == slave_id)
                    {
                        link_cost = M[from][to];
                        popularity = P[from];
                        source = from;
                        dest = to;
                        inserted = true;
                        break;
                    }
                }
            }
        }
        assert(inserted);
        //Send link cost, popularity, src, dest
        MPI_Send(&link_cost, 1, MPI_INT, slave_id, 0, MPI_COMM_WORLD);
        MPI_Send(&popularity, 1, MPI_FLOAT, slave_id, 0, MPI_COMM_WORLD);
        MPI_Send(&source, 1, MPI_INT, slave_id, 0, MPI_COMM_WORLD);
        MPI_Send(&dest, 1, MPI_INT, slave_id, 0, MPI_COMM_WORLD);
    //fprintf(stdout, "[%3d]+++ Master : %2d->%2d Initialized link_cost: %2d popularity %f\n", -1, source, dest, link_cost, popularity);
    }
    
}

/*
*   Send t_table to slave processes 
*/
void master_send(int *buffer, int tag)
{

    int chunk = 2;
    for (int slave_id = 1; slave_id < world_size; slave_id++)
    {   
        int head = 0;
        for (int i =0; i < t_table.size(); i++)
        {   
            Li *item = &t_table[i];
            if (item->status != SEND) continue;
            if (item->send_time > 0) continue;
            //fprintf(stdout, "[%3d]+++ Master : List-item(%d) LINKS[%d][%d] == %d  vs slave_id == %d\n", tag, i, item->line, item->index, LINKS[item->line][item->index], slave_id);
            
            if (slave_id == LINKS[item->line][item->index])
            {   
                IF_DEBUG(fprintf(stdout, "[%3d]+++ Master : Send  (%c%d) \n", tag, (char)LINE[get_t_line(i)], get_local_tid(i));)
                item->status = WAIT;
                buffer[head] = i;
                buffer[head+1] = t_table[i].i_tot;
                head +=chunk;
            }
        } 
        
        // Head decides size of buffer 
        MPI_Send(&head, 1, MPI_INT, slave_id, tag, MPI_COMM_WORLD);
        if ( head > 0 )
        {
            MPI_Send(buffer, head, MPI_INT, slave_id, (tag+OFFSET), MPI_COMM_WORLD);
            //fprintf(stdout, "[%3d]+++ Master : Send %2d trains to slave (%2d) \n",tag,  head / chunk, slave_id);
        }
    }
}

/*
* Recieve messages with chunk size 4
*/
void master_recieve(int *buffer, int tag)
{
    int chunk = 4;
    int size = 0;
    MPI_Status mpi_status;

    for (int slave_id = 1; slave_id < world_size; slave_id++)
    {
        MPI_Recv(&size, 1, MPI_INT, slave_id, tag, MPI_COMM_WORLD, &mpi_status);
        if (size > 0)
        {
            MPI_Recv(buffer, size, MPI_INT, slave_id, (tag + OFFSET), MPI_COMM_WORLD, &mpi_status);
            stringstream ss(" ");
            ss << "[" << setw(3) << tag << "]+++ Master : from ("<< setw(2) << slave_id <<") "; 
            for (int head = 0; head < size; head += chunk)
            {
                int id = buffer[head];
                int from = buffer[head + 1];
                int to = buffer[head + 2];

                int status = buffer[head + 3];
                t_table[id].status = status;
                t_table[id].from = from;
                t_table[id].to = to;
                if (status == ARRIVED)
                {
                    t_table[id].i_tot++;
                    t_table[id].from = to;
                    int idx = t_table[id].index;
                    int line = t_table[id].line;
                    t_table[id].index = (idx + 1) % LINKS[line].size();
                    t_table[id].status = SEND;
                }

                ss << " {id: " <<  (char)LINE[get_t_line(id)] << get_local_tid(id) << "-s" <<setw(2) << std::left << from << " -> s" << setw(S/10) << std::left << to << " " <<setw(S/10) << eEnum[status] << " "<< t_table[id].i_tot << "},";
                t_table[id].print = true;
            }
            string line;
            getline(ss, line, '\n');
            IF_DEBUG(fprintf(stdout, "%s \n", line.c_str());)
        }
        else
        {
            ;
            //fprintf(stdout, "[%3d]---   (%2d) : Recieved empty message\n", tag, pid);
        }
    }
}

void master_print(int tic)
{
    stringstream ss("");
    ss << tic << ": ";
    for (int i = 0; i < t_table.size(); i++)
    {   
        Li *item = &t_table.at(i);
        if (item->print != true) 
        {
            IF_DEBUG(ss << "---------, ";)
            continue;
        }
        //item->print = false;


        int train_id = 0;
        ss << (char)LINE[item->line] << get_local_tid(i) << "-s" << setw(S/10) << std::left << item->from;
        if ( item->status == MOVING )
        {
            ss << "->s" << setw(S/10) << std::left << item->to;
        }
        else{
            ;
            IF_DEBUG(ss << "----";)
        }
        ss << ", ";
    }
    string line;
    getline(ss, line, '\n');
    fprintf(stderr, "%s \n", line.c_str());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// SLAVE FUNC ////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
* Initialize by recieving to MPI_send from master
*/
void slave_init() 
{
    selector = 0;
    slot_selector = 0;
    MPI_Status mpi_status;
    MPI_Recv(&link_cost, 1, MPI_INT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
    MPI_Recv(&popularity, 1, MPI_FLOAT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
    MPI_Recv(&source, 1, MPI_INT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
    MPI_Recv(&dest, 1, MPI_INT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);

    IF_DEBUG(fprintf(stdout, "[%3d]---   (%2d) : %2d->%2d Initialized link_cost: %2d popularity %f\n", -1, pid, source, dest, link_cost, popularity);)
}

void slave_recieve(int *buffer, int tag)
{
    int chunk = 2;
    int size = 0;
    MPI_Status mpi_status;

    MPI_Recv(&size, 1, MPI_INT, MASTER, tag, MPI_COMM_WORLD, &mpi_status);
    if (size > 0)
    {
        MPI_Recv(buffer, size, MPI_INT, MASTER, (tag + OFFSET), MPI_COMM_WORLD, &mpi_status);
        stringstream ss(" ");
        ss << "[" << setw(3) << tag << "]---   (" << setw(2) << pid << ") : Recieved ";
        for (int i = 0; i < size; i += chunk)
        {
            ss  << ", (" << (char)LINE[get_t_line(buffer[i])] << get_local_tid(buffer[i]) << ") ";
            int w_time;
            bool print = false;
            if (first_run)
            {
                w_time = 1;
                print = true;
            }   
            else
            {
                w_time = ceil((float)((rand()%10) + 1)* popularity);
            }
            
            // Add train in the correct queue;
            Train train(buffer[i], buffer[i+1], w_time, WAIT_SLOT, print);
            queue.at(get_t_line(buffer[i])).push_front(train);
        }
        string line;
        getline(ss, line, '\n');
        IF_DEBUG(fprintf(stdout, "%s \n", line.c_str());)
    }
    else
    {
        ;
        //fprintf(stdout, "[%3d]---   (%2d) : Recieved empty message\n", tag, pid);
    }
}

void slave_send(int *buffer, int tag)
{
    int chunk = 4;
    int head = 0;
    for (int i = 0; i < queue.size(); i++)
    {
        for (vector<deque<Train> >::iterator it1 = queue.begin(); it1 != queue.end(); ++it1)
        {
            for (deque<Train>::iterator item = it1->begin(); item != it1->end();)
            {
                //  Train item = queue.at(i).at(j);
                if (item->send)
                {
                    buffer[head] = item->id;
                    buffer[head + 1] = source;
                    buffer[head + 2] = dest;
                    buffer[head + 3] = item->status;
                    head += chunk;
                    item->send = false;
                }

                // erase safely from deque
                if (item->status == ARRIVED)
                    it1->erase(item);
                else
                    item++;
            }
        }

        // Head decides size of buffer
        MPI_Send(&head, 1, MPI_INT, MASTER, tag, MPI_COMM_WORLD);
        if (head > 0)
        {
            MPI_Send(buffer, head, MPI_INT, MASTER, (tag + OFFSET), MPI_COMM_WORLD);
            IF_DEBUG(fprintf(stdout, "[%3d]---   (%2d) : Send %2d trains to MASTER \n", tag, pid, head / chunk);)
        }
    }
}

void slave_tic(int tic)
{
    // Slot selector handle that incoming trains has to wait until a slot becomes available 
    // at the station.
    bool found = false;
    for (int i = 0; i < ROUTES; i++)
    {
        for (std::deque<Train>::reverse_iterator it = queue.at(slot_selector).rbegin(); it != queue.at(slot_selector).rend(); ++it)
        {
            // Slot selector  will not change value when train has open doors, therefore it must wait
            if (it->status == WAIT_SLOT || it->status == OPEN_DOORS)
            {
                // If it enters this if case the train is assigned slot at station (Can only be one train in the slot at any given time) 
                if (it->status == WAIT_SLOT)
                {
                    slave_stop_clock(tic, it);
                }

                found = true;
                it->status = OPEN_DOORS;

                if ( --it->w_time < 0 )
                {
                    it->status = CLOSE_DOORS;
                    it->w_time = link_cost;
                    slot_selector = (slot_selector + 1) % ROUTES;
                }
                break;
            }
        }
        // Only two case's we want to change slot_selector is 
        //    1) when train CLOSE_DOORS 
        //    2) when no train with status OPEN_DOORS was found 
        if ( found )
            break;
        slot_selector = (slot_selector + 1) % ROUTES;
    }


    // selector handle which train that are travelling on link
    // -- Loop works in the same way as slot_selector loop
    found = false;
    for (int i = 0; i < ROUTES; i++)
    {
        for (std::deque<Train>::reverse_iterator it = queue.at(selector).rbegin(); it != queue.at(selector).rend(); ++it)
        {
            // selector cant change value until link has been travelled 
            if (it->status == CLOSE_DOORS || it->status == MOVING)
            {
                if (it->status == CLOSE_DOORS)
                {
                    it->send = true;
                    slave_start_clock(tic, it);
                }

                found = true;
                it->status = MOVING;
                if ( --it->w_time < 0 && it->status != ARRIVED)
                {
                    it->status = ARRIVED;
                    it->send = true; 
                    selector = (selector + 1) % ROUTES;
                }
                break;
            }
        }
        if ( found )
            break;
        selector = (selector + 1) % ROUTES;
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// WAIT TIME FUNC  ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void slave_start_clock(int tic, std::deque<Train>::reverse_iterator it)
{
    int line = get_t_line(it->id);
    if ( it->i_tot > 0)
        myclock[line] = tic;
}

void slave_stop_clock(int tic, std::deque<Train>::reverse_iterator it)
{
    int old_tic = myclock[get_t_line(it->id)];
    int line = get_t_line(it->id);
    if ( old_tic == 0 || it->i_tot == 0)
    {
        return;
    }
    else
    {
        w_avr_sum[line] += tic - old_tic;
        w_avr_count[line]++;

        if (w_longest[line] < (tic - old_tic))
            w_longest[line] = tic - old_tic;
        if (w_shortest[line] > (tic - old_tic))
        {
            w_shortest[line] = tic - old_tic;
           // fprintf(stdout, "[%3d]---   (%2d) : New shortest %d\n", tic, pid, tic - old_tic);
        }
    }
}


void compute_avg(float result[ROUTES], float sum_vec[ROUTES], float div_vec[ROUTES], bool divide, bool gt_zero_constraint)
{
    float process_avg[3] = {0,0,0};
    float process_missing[3] = {0,0,0};
    
    for ( int i =0; i < ROUTES; i++)
    {   
        // Watch out for zero division, if zero or INF in data adjust divider process_missing
        if (sum_vec[i] > 0 && gt_zero_constraint)         
            process_avg[i] = sum_vec[i] / (divide ? div_vec[i] : 1);
        else if (sum_vec[i] < INF && !gt_zero_constraint)
            process_avg[i] = sum_vec[i] / (divide ? div_vec[i] : 1);
        else 
            process_missing[i]++;
    }

    // Collect all vector by MPI_SUM operation 
    MPI_Reduce(process_avg, sum_vec, 3, MPI_FLOAT, MPI_SUM, 0,MPI_COMM_WORLD);
    MPI_Reduce(process_missing, div_vec, 3, MPI_FLOAT, MPI_SUM, 0,MPI_COMM_WORLD);
    
    for (int i = 0; i < ROUTES; i++) 
    {
        result[i] = sum_vec[i] / ((float)world_size - div_vec[i]);
    }
}


//
//  MAIN Function
// 
int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int buffer[100];
    int tic = 0;

    // Initialize master and slave processes
    if (pid == MASTER)
    {
        IF_DEBUG(fprintf(stdout, "[tic]   <pid>   : <message>\n");
                 fprintf(stdout, "___________________________________________________________________________\n");
                 fprintf(stdout, "[%3d]+++ Master : Started\n", tic);)
        // Initialize  master process
        master_parse_input();
        master_init();
    }

    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);                   // Send max iteration to every process
    MPI_Bcast(&T_trains, ROUTES, MPI_INT, 0, MPI_COMM_WORLD);       // Send max train_routes to every process
    MPI_Barrier(MPI_COMM_WORLD);

    if (pid == MASTER)
    {
         // Send trains to links but first sync
        MPI_Barrier(MPI_COMM_WORLD);
        master_send(buffer, tic);
    }
    else if ( pid != MASTER )
    {
        slave_init();
        MPI_Barrier(MPI_COMM_WORLD);                                // All slaves has to be initialized before recieve
        slave_recieve(buffer, tic);
    }


    //
    // Run simulation
    //
    while ( tic < N)
    {
        if (pid == MASTER)
        {
            for (int i = 0; i < t_table.size(); i++)
            {
                Li *item = &t_table.at(i);

                // Add tickdown effect
                if (item->status == INIT)
                {
                    if (--(item->send_time) <= 0)
                        item->status = SEND;
                }
            }

            master_recieve(buffer, tic);
            master_send(buffer, tic);
            master_print(tic);
        }
        else
        {   
            slave_tic(tic);
            slave_send(buffer, tic);
            slave_recieve(buffer, tic);
            first_run = false;
        }
        tic++;
    }
    //
    // End simulation
    //

    IF_DEBUG(fprintf(stdout, "[%3d]---   (%2d) : AverageCount  %3.1f, %3.1f, %3.1f\n", tic,pid,  w_avr_count[0], w_avr_count[1], w_avr_count[2]);)
    MPI_Barrier(MPI_COMM_WORLD);

    // Compute waiting times
    float tot_avg[3];
    compute_avg(tot_avg, w_avr_sum, w_avr_count, true, true);
    MPI_Barrier(MPI_COMM_WORLD);

    float short_avg[3];
    compute_avg(short_avg, w_shortest, w_avr_count, false, false);
    MPI_Barrier(MPI_COMM_WORLD);

    float long_avg[3];
    compute_avg(long_avg, w_longest, w_avr_count, false, true);
    MPI_Barrier(MPI_COMM_WORLD);
    
    // Print the result
    if (pid == MASTER)
    {
        IF_DEBUG(fprintf(stdout, "+++ Master : __________________________\n");fprintf(stdout, "+++ Master : |_row_|ticks|_Line_|_InDx_|Status|\n");for (int i = 0; i < t_table.size(); i++) fprintf(stdout, "+++ Master :%4d: | %3d  | %3d  | %3d  | %3d  |\n", i, t_table.at(i).i_tot, t_table.at(i).line, t_table.at(i).index, (int)t_table.at(i).status);)
        
        fprintf(stdout, "Average waiting times:\n");
        for (int i = 0; i < ROUTES; i++)
        {
            fprintf(stdout, "%s: %d trains -> %2f, %2f, %2f\n", Lines[i].c_str(), T_trains[i], tot_avg[i], short_avg[i], long_avg[i]);
        }
    }
    
    MPI_Finalize();
    return 0;
}
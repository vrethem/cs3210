#!/bin/bash
echo "Bash version ${BASH_VERSION}..."
rm -f log.txt
rm -f slog.txt

# Compile program 
mpic++ script_task2.cpp -o task2script -std=c++17

if [ $# -eq 0 ]
  then
    echo "No arguments gives, rerun with ./script_task2.sh 100"
    exit
fi
N=$1

trains=1
old_val=0
new_val=1
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!!!Ticks - the total number of times all trains switched station during last  !!!!!!"
echo "!!!!      - iteration. Counts when a train arrives at a station.               !!!!!!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
ocations=0
while ((3 > ocations));
  do

  if ((new_val > old_val));
  then
    old_val=$new_val
    if ((0 > ocations));
    then
      ocations=$ocations-1
    else
      ocations=0
    fi
  else
    ocations=$ocations+1
fi

  trains=$(( 1+trains )) # trains++
 
  sed -e "s/\${N}/$N/" -e "s/\${G}/$trains/" -e "s/\${Y}/0/" -e "s/\${B}/0/" script_input.txt > tmp.txt

  echo " >Simulating with " $N "iterations, G="$trains", Y="0", B="0":"  
  mpirun -np 17 ./task2script < tmp.txt 2> log.txt 2&>> slog.txt
  new_val=$?
  echo " Ticks == "$new_val
 done

echo " "
 echo $trains " is the max for GREEN line, line 0"
echo " "

 ### YELLOW LINE 
trains=1
old_val=0
new_val=1
 ocations=0
while ((3 > ocations));
  do

  if ((new_val > old_val));
  then
    old_val=$new_val
  else
    ocations=$ocations+1
fi

  trains=$(( 1+trains )) # trains++
 
  sed -e "s/\${N}/$N/" -e "s/\${G}/0/" -e "s/\${Y}/$trains/" -e "s/\${B}/0/" script_input.txt > tmp.txt

  echo " >Simulating with " $N "iterations, G="0", Y="$trains", B="0":"  
  mpirun -np 17 ./task2script < tmp.txt 2> log.txt 2&>> slog.txt
  new_val=$?
  echo " Ticks == "$new_val
 done

echo " "
 echo $trains " is the upper bound for YELLOW line, line 0"
echo " "

 ### BLUE LINE 
 trains=1
 old_val=0
new_val=1
 ocations=0
while ((3 > ocations));
  do

  if ((new_val > old_val));
  then
    old_val=$new_val
  else
    ocations=$ocations+1
fi

  trains=$(( 1+trains )) # trains++
 
  sed -e "s/\${N}/$N/" -e "s/\${G}/0/" -e "s/\${Y}/0/" -e "s/\${B}/$trains/" script_input.txt > tmp.txt

  echo " >Simulating with " $N "iterations, G="0", Y="0", B="$trains":"  
  mpirun -np 17 ./task2script < tmp.txt 2> log.txt 2&>> slog.txt
  new_val=$?
  echo " Ticks == "$new_val
 done

echo " "
 echo $trains " is the upper bound for BLUE line, line 0" 
echo " "


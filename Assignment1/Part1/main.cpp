/*
    Thew program does not consists of queue's
    A link choices which train to dispatch according to longest waiting timein 'km' variable (Whihc will be negative)
*/

#include <omp.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <numeric>

#include "Node.h"
void seqential_print_order(Train *t, vector<string> *table);
void print_to_console(Train *t);
void populate_link_n_nodes();
Link* create_link(int from, int to, int cost);


int main() {
    srand(1); // Pseudorandom
    read_input();
    populate_link_n_nodes();


    Train *t;
    int tid = 0;
    int iteration = 0;
    int sum = accumulate(Threads.begin(), Threads.end(), 0);
    vector<string> table(sum); 
#pragma omp parallel num_threads(sum) shared(S, N, iteration, table) private(t, tid)
    {
        tid = omp_get_thread_num();
        t = new Train();
        int sum = 0;
        for (int i = 0; i < Threads.size(); i++)
        {
            sum += Threads[i];
            if (tid < sum)
            {
                t->route = i;
                break;
            }
        }
        if (tid%2)
        {
            t->node = nodes.at(R[t->route][0]);
        }
        else
        {
            int last_idx = R[t->route].size() -1;
            t->node = nodes.at(R[t->route][last_idx]);
            t->dir *= -1;
        }
        t->km   = 1 + t->tid() / 2;
        
        #pragma omp barrier

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////// Start simulaion ///////////////////////////////////////////////////////////////
        while (iteration <= N)
        {
#pragma omp single
            cerr << endl << iteration << ": ";

            if (t->km <= 0 && t->w <= 0)
            {
                t->link = next_link(t);
                queue(t);
            }
            for (Node *n : nodes)
            {
                n->free_slot = true;
            }        
#pragma omp barrier


#pragma omp critical // Needed to sync links
            if (t->link != NULL)
            {
                // Thread has access to Link == Thread will start travel on link next tic()
                if (t == t->link->t && t->km <= 0 && t->w <= 0 && t->node->free_slot == true)
                {
                    t->node->free_slot = false;
                    t->km = t->link->cost+1;
                    // Reset wait time when train leaves (start the clock)
                    t->link->current_w_time[t->route] = iteration;
                }
            }

            tic(t, iteration);

#pragma omp barrier
            //seqential_print_order(t, &table);
            print_to_console(t);

#pragma omp single
            iteration++;
#pragma omp barrier
        }
        //////////////////////////////////// End simulation ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        

// Cleanup 
        delete t;
        t = NULL;
    }


    cout << endl
         << "Average waiting time:" << endl;
    for (int route = 0; route < ROUTES; route++)
    {
        print_average(route);
    }

    // Free memory
    for ( Node * ptr : nodes ) {
        ptr->free_mem();
        delete ptr;
        ptr = NULL;
    }
}


Link* create_link(int from, int to, int cost) {
        Link *link = new Link();
        link->from = nodes[from];
        link->to = nodes[to];
        link->cost = cost;
        return link;
}

void populate_link_n_nodes() {
    for (int r=0; r < S; r++) 
        nodes.push_back(new Node(Node{r, P[r]}));
    
    for (int r=0; r < S; r++) {
        Node *node = nodes[r];
        node->free_slot = true;
        for (int c=0; c < S; c++) 
            if ( M[r][c] > 0 ) 
                node->links.push_back(create_link(r, c, M[r][c]));
    }
}

void add_wait_times(Train *t, int diff) {
    if (diff < 3) return;
    t->link->w_avr_count[t->route]++;
    t->link->w_avr_sum[t->route] += diff;
    if (t->link->w_longest[t->route] < diff)
        t->link->w_longest[t->route] = diff;
    if (t->link->w_shortest[t->route] > diff)
        t->link->w_shortest[t->route] = diff;
}

/*
* queue will always select one of the train with the least value of t->km
*/
void queue(Train *t) {
    Link *l = t->link;
    if (l->t == NULL)
    {
        l->t = t;
    }
    else
    {
#pragma omp critical
        {
            if (l->t->km <= 0 && l->t->km > t->km)
            {
                l->t = t;
            }
        }
    }
}


/*
* t->km > 0 then the train is ethier in transit or loading
* t->node changes when train has travelled cost on Link
*/
void tic(Train *t, int i) {
    t->w--;
    t->km--;
    if (t->km == 0) {
        t->print = true;
        if ( t->link != NULL) {
            t->node = t->link->to;
            t->link->t = NULL;
            t->w = (int)ceil(t->node->popularity * (rand()%10 +1));
            if (end_station(t->route, t->node)) {
                t->dir *= -1;
            }
            add_wait_times(t, i - next_link(t)->current_w_time[t->route]);
        }
    }
}

Link* next_link(Train *t) {
    for (int i=0; i < t->node->links.size(); i++) {
        Link *l = t->node->links.at(i);
        if ( l->to == t->next_node() ) {
            return l;
        }
    }
    quit("next link to train->node[" +to_string(t->node->idx)+ "] not found");
}


bool end_station(int route, Node *n) {
        vector<int> v = R.at(route);
        return (n == nodes.at(v[0]) || n == nodes.at(v[v.size()-1]) );
}


void print_to_console(Train *t)
{
    /*  Random print order, because of concurrecncy */
#pragma omp critical
    {
        if (t->link != NULL)
        {
            if (t == t->link->t)// && t->km == t->link->cost && t->w <= 0)
            {
                cout << t->color() << t->tid() << "-s" << t->node->idx << (t->km >= 0 ? "->s" + to_string(t->link->to->idx) : "") << ", ";
            }
            else if (t->print)
            {
                cout << t->color() << t->tid() << "-s" << t->node->idx << ", ";
            }
        }
        else if (t->print)
        {
            cout << t->color() << t->tid() << "-s" << t->node->idx << ", ";
        }
    }
}

void print_average(int route) {
    float x1=0,x2=0,x3=0;
    int check=0;
    int k =0;
    for (Node * n : nodes ) {
        for (Link * l : n->links ) {
            if ( l->w_avr_count[route] != 0 ) {
                check++;
                k += l->w_avr_count[route];
                x1 += l->w_avr_sum[route] / l->w_avr_count[route];
                //cout << "\t\t -|"<<Lines[route] << " link["<< L[l->from->idx] <<"->"<< L[l->to->idx] << "] been visited " << l->w_avr_count[route] << " times "<< endl;
            }
            if ( l->w_longest[route] != -1) {
                x2 += l->w_longest[route];
            }
            if ( l->w_shortest[route] != 99) {
                x3 += l->w_shortest[route];
            }
        }
    }
    x1 /= (float)(R[route].size()*2 -2);
    x2 /= (float)(R[route].size()*2 -2);
    x3 /= (float)(R[route].size()*2 -2);
    //cerr << "\t\t -|Total of "<< check <<" links have been visited " << k << " times "<< endl;
    cout.precision(1);
    cout << std::fixed << Lines[route] << ": " << Threads[route] << " trains -> " << x1 << ", " << x2 << ", " << x3 << endl;
}


void quit(string s) {
    cerr <<  s << endl; 
    exit(1);
}
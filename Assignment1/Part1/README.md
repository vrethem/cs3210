#To run the code and observe the output you need to compile the code provided by running: 
 g++ main.cpp -o task1 -std=c++11 -fopenmp  
#Then run the compiled file as usual: 
 ./task1 < input.txt
#Also, there’s a script provided for running the code automatically on different number of trains. The trains are varied from 2 up to 64 trains per #line. The parameter of the script defines the number N, for example: 
 ./simulation 100
#The program outputs some extra info via the ‘cerr’ stream, this output is piped to the file log.txt to limit the the output to the terminal window.

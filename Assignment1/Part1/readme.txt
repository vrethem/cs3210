Compile with: 
 g++ main.cpp -o task1 -std=c++11 -fopenmp  
Run the file with: 
 ./task1 < input.txt

Also, there’s a script provided for running the code automatically on different number of trains. 
The trains are varied from 2 up to 64 trains per line. The parameter of the script defines the number N, for example: 
 ./simulation 100
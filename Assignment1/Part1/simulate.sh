#!/bin/bash
echo "Bash version ${BASH_VERSION}..."
rm -f log.txt

g++ main.cpp -o task1 -std=c++11 -fopenmp 

if [ $# -eq 0 ]
  then
    echo "No arguments gives, rerun with ./simulate.sh 100"
    exit
fi

LIMIT=64  # Upper limit
trains=1
N=$1
#for trains in {8..64..8}
while ((trains < LIMIT));
  do
  trains=$(( 2*trains )) # Could maybe use step variable to step logarithmticly 

  echo "____________________________________________________________________" 
  echo "Green, Yellow, Blue line = "$trains", "$trains", "$trains 
  echo "____________________________________________________________________" >> log.txt 
  echo "Green, Yellow, Blue line = "$trains", "$trains", "$trains  >> log.txt 
 
#for N in {10..100..90}
#  do 
  sed -e "s/\${N}/$N/" -e "s/\${G}/$trains/" -e "s/\${Y}/$trains/" -e "s/\${B}/$trains/" script_input.txt > tmp.txt

  echo " >Simulating with " $N "iterations, G="$trains", Y="$trains", B="$trains":"  
    echo " >Simulating with " $N "iterations,G="$trains", Y="$trains", B="$trains >> log.txt 
  ./task1 < tmp.txt
  echo ""
 #done
 done
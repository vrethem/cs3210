#!/bin/bash
echo "Bash version ${BASH_VERSION}..."

rm -f out.txt
rm -f log.txt

g++ main.cpp -o task1 -std=c++11 -fopenmp

for i in {10..20..10}
do
    sed -u -e "s/\${N}/$i/g" -e "s/\${G}/$i/g" -e "s/\${Y}/$i/g" -e "s/\${B}/$i/g" script_input.txt > tmp.txt
    
    #cat tmp.txt
    echo "Simulating with " $i "iterations, "  >> out.txt
    ./a.out < tmp.txt >log.txt 2>>out.txt
done
#ifndef PARSER_H
#define PARSER_H
#include <omp.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <iomanip>

#define WORD 10
#define ROUTES 3

using namespace std;

/*
split a line on readable type, delimiter as a character.
*/
template <class T>
vector<T> split_on(string line, char delimeter)
{
    string item;
    std::stringstream ss(line);
    std::vector<T> v;
    while (std::getline(ss, item, delimeter))
    {   
        stringstream s1(item);
        T t;
        s1 >> t;
        v.push_back(t);
    }
    return v;
}

// Read by refrence 
void read_input(int& S, 
                int& N,
                vector<string>& L, 
                vector<vector<int> >& R, 
                int *T_line, 
                vector<float>& P, 
                vector<vector<int> >& M)
{
    string line;
    //ifstream cin("input.txt");
    
    cin >> S;
    getline(cin, line, '\n');
    
    getline(cin, line, '\n');
    L = split_on<string>(line, ',');
    //cerr << "Read stations\n";
	
    // Read Matrix
    for (int r=0; r < S;r++) {
        getline(cin, line, '\n');    
        M.push_back(split_on<int>(line, ' '));
    }
    //cerr << "Read Matrix\n";
	
    getline(cin, line, '\n');
    P = split_on<float>(line, ',');
    //cerr << "Read Popularity\n";
	
    for (int i=0; i < ROUTES;i++) {
        getline(cin, line, '\n');
        vector<string> v = split_on<string>(line, ',');
        vector<int> v2;
        for ( string s : v) {
            for(int i=0; i < S; i++) {
                if (L.at(i) == s) {
                    v2.push_back(i);
                    break;
                }
            }
        }
        R[i] = v2;
    }
    //cerr << "Read routes\n";

    // READ ROUTES
    cin >> N;
    getline(cin, line, '\n');
    getline(cin, line, '\n');
    vector<int> vt = split_on<int>(line, ',');
    for (int i=0; i < vt.size(); i++)
        T_line[i] = vt[i];
    //cerr << "read_input() done" << endl;
 }

 void test_input(int S, int N,
                 vector<string> L,
                 vector<vector<int> > R,
                 int *T_line,
                 vector<float> P,
                 vector<vector<int> > M)
 {
     cerr << S << endl;
     for (int r = 0; r < M.size(); r++)
     {
         cerr << L[r] << ",";
     }
     cerr << endl;
     for (int r = 0; r < M.size(); r++)
     {
         cerr << "[" << r << "]";
         for (int c = 0; c < M.at(r).size(); c++)
         {
             cerr << " " << M.at(r).at(c) << " ";
         }
         cerr << endl;
     }
     for (int r = 0; r < P.size(); r++)
     {
         cerr << P.at(r) << ",";
     }
     cerr << endl;
     for (int r = 0; r < ROUTES; r++)
     {
         for (int c = 0; c < R.at(r).size(); c++)
         {
             cerr << R.at(r).at(c) << ",";
         }
         cerr << endl;
     }
     cerr << N << endl;
     for (int r = 0; r < ROUTES; r++)
     {
         cerr << T_line[r] << ",";
     }
     cerr << endl;
     cerr << "Input test done!\n\n";
 }

#endif
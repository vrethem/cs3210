#ifndef NODE_H
#define NODE_H
#include <omp.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <iomanip>

#define WORD 10
#define ROUTES 3

using namespace std;

class Link;
class Node;
void quit(string s);
vector<vector<Link*> > links;
vector<Node*> nodes;

int S, N;
vector<string> L;
vector<vector<int> > R(ROUTES);
vector<int> Threads;
string Lines[ROUTES] = {"green", "yellow", "blue"};
char LINE[ROUTES] = {'g','y','b'};
vector<float> P;
vector<vector<int> > M;

class Link {
    public:
    ~Link(){};
    Node *from;
    Node *to;
    struct Train *t;
    int cost;
    //omp_lock_t lock;

    float current_w_time[3] = { 0, 0, 0};
    float w_avr_count[3]    = { 0, 0, 0};
    float w_avr_sum[3]      = { 0, 0, 0};
    float w_longest[3]      = {-1,-1,-1}; // Each link can have up to three different wait times (for each MRT-line)
    float w_shortest[3]     = {99,99,99};
};

class Node {
    public:
    ~Node(){};
    int idx;
    float popularity;
    vector<Link*> links;
    bool free_slot; 

    void* free_mem() {
        for ( Link *ptr : this->links ) {
            delete ptr;
            ptr = NULL;
        }
    }
};

struct Train {
    int route;
    Node *node; 
    Link *link = NULL;
    int km  = 1; //cost left on Link, if <= 0 then Train is not on a Link
    int dir = 1;
    int w   = 0;
    bool print = false;
    char color() {
        return LINE[this->route];
    }
    int tid() {
        int sum = 0;
        for ( int i=0; i < Threads.size(); i++) {
            sum += Threads[i];
            if ( omp_get_thread_num() < sum ) { break;}
        }
        return abs( omp_get_thread_num() - sum +1);
    }
    Node* next_node() {
        if ( this->index_on_line() + this->dir < 0 || R[this->route].size() <= this->index_on_line() + this->dir) quit("next_station() outside index " + to_string(this->index_on_line()+this->dir) );
        return nodes[R[this->route].at(this->index_on_line() + this->dir)];
    }
    int index_on_line() {
        for (int i=0; i < R[this->route].size(); i++) {
            if (this->node->idx == R[this->route][i]) {
                return i;
            }
        }
        quit("index_on_line() out of range");;
    }
};
typedef struct Train Train;


int station(char *s);
Link* next_link(Train *t);
void queue(Train *t);
void tic(Train *t, int i);
bool end_station(int route, Node *n);
void print_average(int route);
void add_wait_times(Train *t, int diff);


/*
split a line on readable type, delimiter as a character.
*/
template <class T>
vector<T> split_on(string line, char delimeter)
{
    string item;
    std::stringstream ss(line);
    std::vector<T> v;
    while (std::getline(ss, item, delimeter))
    {   
        stringstream s1(item);
        T t;
        s1 >> t;
        v.push_back(t);
    }
    return v;
}

 void read_input() {
    string line;
    //ifstream cin("input.txt");
    
    cin >> S;
    getline(cin, line, '\n');
    
    getline(cin, line, '\n');
    L = split_on<string>(line, ',');
    //cerr << "Read stations\n";
	
    // Read Matrix
    for (int r=0; r < S;r++) {
        getline(cin, line, '\n');    
        M.push_back(split_on<int>(line, ' '));
    }
    //cerr << "Read Matrixes\n";
	
    getline(cin, line, '\n');
    P = split_on<float>(line, ',');
    //cerr << "Read Popularity\n";
	
    for (int i=0; i < ROUTES;i++) {
        getline(cin, line, '\n');
        vector<string> v = split_on<string>(line, ',');
        vector<int> v2;
        for ( string s : v) {
            for(int i=0; i < S; i++) {
                if (L.at(i) == s) {
                    v2.push_back(i);
                    break;
                }
            }
        }
        R[i] = v2;
    }
    //cerr << "Read routes\n";

    // READ ROUTES
    cin >> N;
    getline(cin, line, '\n');
    getline(cin, line, '\n');
    Threads = split_on<int>(line, ',');
    //cerr << "read_input() done" << endl;
 }

 void test_input() {
    cerr << S << endl;
    for(int r=0; r<M.size();r++) {
        cerr << L[r] << ",";
    }
    cerr << endl;
    for(int r=0; r < M.size();r++) {
        cerr << "[" << r << "]";
        for(int c=0; c < M.at(r).size();c++) {
            cerr << " " << M.at(r).at(c) << " ";
        }
        cerr << endl;
    }
    for(int r=0; r<P.size(); r++) {
        cerr << P.at(r) << ",";
    }
    cerr << endl;
    for(int r=0; r<ROUTES; r++) {
        for(int c=0; c < R.at(r).size(); c++) {
            cerr << R.at(r).at(c) << ",";
        }
        cerr << endl;
    }
    cerr << N << endl;
    for(int r=0; r<ROUTES; r++) {
        cerr << Threads.at(r) << ",";
    }cerr << endl;
    cerr << "Input test done!\n\n";
 }

void test_allocated_mem() {
    for (int i=0; i < S; i++) {
        cerr << "L[" << nodes[i]->idx << "] == " << L.at(i) << endl; //<< (nodes[i]->end_station ? "\t is end station" : "") << endl;
        for ( auto link : nodes[i]->links ) {
            cerr << setw(10) << L[link->from->idx] << " -> " << setw(10) << L[link->to->idx] << "  \t cost " << link->cost  << endl;
        }
    }
    cerr << "Test of allocated mem Done! " << endl << endl;
}

#endif
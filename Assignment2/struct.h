#ifndef __STRUCT_H__
#define __STRUCT_H__

#include <stdint.h> // For uint8_t

/*
* Data structures 
*/
union Hash {
    uint8_t byte[52];
    struct
    {
        uint32_t t;
        uint64_t prev_digest[4];
        uint64_t id;
        uint64_t nonce_Little_End;
    } __attribute__((packed)); // Ensure byte array conforms with struct
} __attribute__((packed));
typedef union Hash Hash;

#endif
#ifndef __PARSER_H__
#define __PARSER_H__
#include <iostream> // cout, cin
#include <stdint.h> // For uint8_t
#include <string.h> //
#include <ctime>    // For epoch
#include <sstream>  // for int to hexString conversion
#include <stdlib.h>
#include <string> // use to_string()
#include <bitset> // Debug with bitset
#include <iomanip>
#include <cmath>
#include <assert.h>
#include "struct.h"
#include "definitions.h"

/*
 * Check if Big or Little endian???
 */
void check_endianess()
{
    uint8_t ptr[8] = {0x00, 0x00, 0x35, 0x63, 0x67, 0x3b, 0xfa, 0x11};
    uint64_t *ptr64 = (uint64_t *)ptr;
    printf("uint8_t reads [");
    for (int i = 0; i < 8; i++)
        printf("%02x", ptr[i]);
    printf("] uint64_t reads [%016llx]\n", *(unsigned long long int *)ptr64);
    if (*ptr == *ptr64 >> sizeof(uint8_t) * 8)
        printf("¤¤¤¤¤¤ HOST USES BIG ENDIAN  ¤¤¤¤¤¤ \n");
    else
        printf("¤¤¤¤¤¤ HOST USES LITLLE ENDIAN ¤¤¤¤¤ \n");
}

/*
 * Converts a hex string to uint8_t
 */
void hexStrToUint8(std::string hex, uint8_t *out)
{
    for (int i = 0; i < hex.length(); i += 2)
    {
        int val = std::stoi(hex.substr(i, 2).c_str(), 0, 16);
        uint8_t u_val = 0;
        u_val |= val;

        out[i / 2] = u_val;
        IF_DEBUG(std::cout << std::bitset<8>((unsigned int)u_val);)
    }
    IF_DEBUG(std::cout << std::endl;)
}

/**
 * * PARSE  INPUT 
 */
void parse_input(Hash *host_X)
{
    IF_DEBUG(check_endianess();)
    std::string t_hex;
    std::string d_hex;
    std::string id_ascii, id_hex;
    std::string n_str, n_hex;
    unsigned long long int n_ull;

    //////////////////////////
    // Hexstring to uint8_t
    std::getline(std::cin, d_hex);

    //d_hex = "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71";
    hexStrToUint8(d_hex, (uint8_t *)&host_X->prev_digest);

    //////////////////////////
    // Convert decimal string to uint8_t
    std::getline(std::cin, n_str);

    //n_str = "281474976710656";
    { // Read in string as a decimal number
        std::stringstream ss;
        ss << n_str;
        ss >> n_ull;
        IF_DEBUG(std::cout << n_ull << std::endl;)
    }
    { // Convert deciaml into hex string
        std::stringstream ss;
        ss << std::setfill('0') << std::setw(sizeof(host_X->nonce_Little_End) * 2) << std::hex << n_ull;
        n_hex = std::string(ss.str());
        IF_DEBUG(std::cout << n_hex << std::endl;)
    }
    hexStrToUint8(n_hex, (uint8_t *)&host_X->nonce_Little_End);

    //////////////////////////
    // Convert text to ASCII hex
    id_ascii = NUSNET_ID;
    {
        for (int i = 0; i < 8; i += 1)
        { // Convert to ASCII
            int num = (int)id_ascii[i];
            std::string tmp = std::to_string(num / 16) + std::to_string(num % 16);
            // Convert to hex
            id_hex += "  ";
            id_hex[(i * 2)] = tmp[0];
            id_hex[(i * 2) + 1] = tmp[1];
        }
    }
    hexStrToUint8(id_hex, (uint8_t *)&host_X->id);
}

#endif
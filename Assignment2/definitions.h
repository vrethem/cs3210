#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

//#define DEBUG // can be defined at compile-time by -DDEBUG flag
#ifdef DEBUG
#define IF_DEBUG(...) __VA_ARGS__
#else
#define IF_DEBUG(...)
#endif

#ifdef TEST
#define IF_TEST(...) __VA_ARGS__
#else
#define IF_TEST(...)
#endif

/*
 * Returns swapped value, does not change bytes in memory
 */
#if (IS_BIG_ENDIAN == 0)
#define Swap2Bytes(val) \
    ((((val) >> 8) & 0x00FF) | (((val) << 8) & 0xFF00))
#define Swap4Bytes(val)                                           \
    ((((val) >> 24) & 0x000000FF) | (((val) >> 8) & 0x0000FF00) | \
     (((val) << 8) & 0x00FF0000) | (((val) << 24) & 0xFF000000))
#define Swap8Bytes(val)                                                            \
    ((((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
     (((val) >> 24) & 0x0000000000FF0000) | (((val) >> 8) & 0x00000000FF000000) |  \
     (((val) << 8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) |  \
     (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000))
#else
#define Swap2Bytes(val) \
    (val)
#define Swap4Bytes(val) \
    (val)
#define Swap8Bytes(val) \
    (val)
#endif

#ifndef NUSNET_ID
#define NUSNET_ID "E1234567"
#endif

#endif
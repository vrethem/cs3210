////////////////////////////////////////////
// CS3210 - Assignment 2 
// Anders Vrethem, Ruslan
//
// See definitions.h for helper macros and 
//  union Hash datastructure  
//
// IF_DEBUG( .. ) is a wrapper for debugging
//                enable code by adding -DDEBUG flag at compile time
// gpuErrchk( .. ) is a wrapper for cudaError's 
#define IS_BIG_ENDIAN 0
#define NUSNET_ID "E1234567"

#include "definitions.h" 
#include "hash.h"
#include "struct.h"
#include "parser.h"

#define gpuErrchk(ans)                        \
    {                                         \
        gpuAssert((ans), __FILE__, __LINE__); \
    }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort)
            exit(code);
    }
}

using namespace std;



__device__ int threadsPerBlock()  {    return blockDim.x * blockDim.y * blockDim.z;                                     }
__device__ int threadsPerRow()    {    return blockDim.x * blockDim.y;                                                  }
__device__ int threadNumInBlock() {    return threadIdx.y + blockDim.y * threadIdx.x + threadIdx.z * threadsPerRow();   }
__device__ int blocksPerRow()     {    return gridDim.x * gridDim.y;                                                    }
__device__ int blocksPerGrid()    {    return gridDim.x * gridDim.y * gridDim.z;                                        }
__device__ int blockNumInGrid()   {    return blockIdx.y  + gridDim.y  * blockIdx.x + blockIdx.z * blocksPerRow();      }
__device__ int tid()              {    return threadNumInBlock();                                                       }
__device__ int global_tid()       {    return blockNumInGrid() * threadsPerBlock() + threadNumInBlock();                }
__device__ int threads()          {    return threadsPerBlock() * blocksPerGrid();                                      }


__device__ int keep_running = 1;

/* Main kernel function
 * 
 * --- RETURNS      [ nonce_Little_End  ,  digest  ]    1D- array             
 * uint8_t successor[ 8      +  32      ]
 *                    64     +  256  bits
 */
__global__ void kernel(uint8_t *dev_X_in, uint8_t *dev_result)
{   
    //////////////////////////
    // Global memory on device
    // keep_running
    // *dev_X_in
    // *dev_result
     
    //////////////////////////
    // Shared memory per block
    __shared__ int shared_keep_running; 

    //////////////////////////
    // Local Memory per thread
    Hash instance;
    Hash *X = &instance;  
    uint8_t result[32];
    uint64_t *ptr64 = NULL;
    unsigned long long int iteration = 0; 
    uint64_t local_n;

    Hash *ptr = (Hash *)dev_X_in;
    memcpy(&local_n, &ptr->nonce_Little_End, 8);

    if (tid() == 0)   
    {
        shared_keep_running  = 1;
    }    
      
    //__syncthreads();
    // Memcpy  to local 
    memcpy(&X->byte, dev_X_in, 44);

// DEBUG  SECTION |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||Test grid sizes
IF_DEBUG( 
    __shared__ uint64_t shared_threads_count;
 //   unsigned long long int iteration = 0; // better to have in local memory then having divergent flow in While loop
    if (tid() == 0) 
        shared_threads_count = 0;
    __syncthreads();

    // Check that all tids are in the game
    atomicAdd((unsigned long long int *)&shared_threads_count, 1);

    __syncthreads();
    if (tid() == 0) {
        printf("(%4d) Count threads in block:  %llu VS %llu \n", global_tid(), shared_threads_count, threadsPerBlock());
        assert ( shared_threads_count == threadsPerBlock() );
    }
)// ||||||||| Test local memory 
IF_DEBUG(
    __syncthreads();

   /* printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) [%6u] nonce =%016llx local_n =%016llx\n", 
    blockIdx.x,blockIdx.y,blockIdx.z,
    threadIdx.x,threadIdx.y,threadIdx.z,
    tid(), Swap8Bytes( X->nonce_Little_End ), Swap8Bytes( local_n ) );
    */
    __syncthreads();
    if (global_tid() == 0)
    {
        printf("--------------------------------------------------------------------------------------------------------\n");
        printf("------------------------------ Test Of Input -----------------------------------------------------------\n");
        printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) GridDim(%2d,%2d,%2d) BlockDim(%2d,%2d,%2d)  tid() == %4u , MAX == %6u blkNumber = %2u\n", 
                blockIdx.x,blockIdx.y,blockIdx.z,
                threadIdx.x,threadIdx.y,threadIdx.z,
                gridDim.x, gridDim.y, gridDim.z,
                blockDim.x, blockDim.y, blockDim.z,
                tid(), threads(), blockNumInGrid() );

        uint8_t ptr[8] = {0x00, 0x00, 0x35, 0x63, 0x67, 0x3b, 0xfa, 0x11}; 
        ptr64 = (uint64_t *)ptr;
        printf("uint8_t reads [");
        for (int i = 0; i < 8; i++)
            printf("%02x", ptr[i]);
        printf("] uint64_t reads [%016llx]\n", *ptr64);
        if (*ptr == *ptr64 >> sizeof(uint8_t) * 8)
            printf("¤¤¤¤¤¤ DEVICE USES BIG ENDIAN  ¤¤¤¤¤¤ \n");
        else
            printf("¤¤¤¤¤¤ DEVICE USES LITLLE ENDIAN ¤¤¤¤¤ \n");
            
        printf("TEST HASH STRING IN  == ");
        for (int i=0; i < 52; i++) printf("%02x", dev_X_in[i]);
        printf("\n");    
        printf("LOCAL   STRING       == ");
        for (int i=0; i < 52; i++) printf("%02x", X->byte[i]);
        printf("\n");
        ptr64 = &local_n;
        printf("SHARED n  base10     == %llu\n", Swap8Bytes(*ptr64));
        printf("SHARED n  base16     == %llx\n", Swap8Bytes(*ptr64));
        uint32_t *ptr32 = (uint32_t*)dev_X_in;
        printf("EPOCH     base10     == %u\n", Swap4Bytes( *ptr32) );
        printf("EPOCH     base16     == %08x\n", Swap4Bytes( *ptr32) );
        
        X->t = Swap4Bytes( 0x5bb16380 );
        X->nonce_Little_End = Swap8Bytes(0xe69d030000000000);
        printf("[Test] with hash X   == ", tid());
        for (int i = 0; i < 52; ++i) printf("%02x", X->byte[i]);
        printf("\n");        
        
        //////////////////////////
        // Calculate 'n' Work
        sha256(result, X->byte, 52);

        printf("SHA256 outputs       == ", tid());
        for (int i = 0; i < 32; ++i) printf("%02x", result[i]);
        printf("\n");
        
        ptr64 = (uint64_t *)result;
        printf("     Result base10   == %llu\n", Swap8Bytes( *ptr64 ) );
        printf("     Result base16   == %016llx\n", Swap8Bytes( *ptr64 ) );
        {
            uint64_t step_size = (UINT64_MAX / ((uint64_t)threads())); // Let each thread start with uniform distance to the next thread
            printf(" STEP_SIZE == 2^64   / %4d == %016llx\n", threads(), step_size);
        }
        printf("--------------------------------------------------------------------------------------------------------\n");
        printf("- Execute Main Loop ----------(Big Endian)-------------(Big Endian)-------------------------------------\n");
        printf("( tid )[  ITERATION NUM ]       NONCE      =SHA256=>    HEX RESULT    ==        DEC RESULT              \n");
        printf("(    0)[               0] 0000000000000000 =SHA256=> 911df0aab1f6e57e == 10456778525745014142           \n");
    }
    __syncthreads();
)//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||



    // Initialize nonce
    // Keep step_size in statement so we don't have to allocate step_size variable through out the looping part
    {
        uint64_t step_size = (UINT64_MAX / ((uint64_t)threads()));      // Let each thread start with uniform distance to the next thread
        X->nonce_Little_End = step_size * global_tid(); 
    }

    __syncthreads();
    int local_keep_running = 1;
    ptr64 = (uint64_t *)result;
    /*
    *  While global var keep_searching is true 
    *   + Avoiding divergent control flow 
    *
    *   Evaluate while condition after because RESULT won't be 
    *   calculated before first run
    *
    *   *result recasted into a uint64_t * for safe conversion 
    */ 
    do
    {
        // Try calculating in batches to minimize access time for global variable 
        IF_BATCH_TEST( for ( int i=0; i < 256 || Swap8Bytes( *ptr64 ) > Swap8Bytes( local_n ); i++) )
        {
	    X->nonce_Little_End++;

	    //////////////////////////
        // Calculate 
        sha256(result, X->byte, 52);

        iteration++; 
        }
        
        // minimize access time for global variable
        if (tid() == 0 ) 
            shared_keep_running = keep_running;  
        local_keep_running = shared_keep_running;
        if ( Swap8Bytes( *ptr64 ) < Swap8Bytes( local_n ) )
        {
             //////////////////////////
            // Get result by Compare And Swap
            //  to ensure mutual access for  
            //  first thread that exits loop
            if ( atomicCAS(&keep_running, 1, 0) )
            {   
                memcpy(dev_result, ((uint8_t*)&X->nonce_Little_End), 8 );
                memcpy(dev_result+sizeof(uint8_t)*8, (uint8_t*)result, 32 );
            IF_DEBUG(
                printf("<<<(%5d)[%16llx]>>> Result: %llu < %llu\n", global_tid(), iteration, Swap8Bytes( *ptr64 ), Swap8Bytes ( local_n ) );
                printf("<<<(%5d)[%16llx]>>> Sending back : [ %016llx , ", global_tid(),  iteration, Swap8Bytes( X->nonce_Little_End ) );
                for (int i= 8; i < 40;i++) printf("%02x", dev_result[i]);
                printf(" ] \n");
                printf("--------------------------------------------------------------------------------------------------------\n");
            )
                shared_keep_running = 0;  
            }
            else 
            {   
                shared_keep_running = 0;  
                ;// Sorry thread you were not first
            }   
        }

// DEBUG  SECTION |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
IF_DEBUG(
        if (tid() == 0)
        {   
            printf("(%5d)[%16llx] %016llx =SHA256=> %016llx == %21llu   Keep_running = %d\n", global_tid(), iteration, Swap8Bytes( X->nonce_Little_End ), Swap8Bytes( *ptr64 ),Swap8Bytes( *ptr64 ), local_keep_running);
        }
)//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    } while ( local_keep_running );
}



int get_argument(char * argument)
{
    std::istringstream iss( argument );
    int val;
    if (iss >> val)
    { // Conversion successful
        return val;
    }
    else 
    {
        cerr << "Invalid conversion of argument" << endl;
        exit(-1);
    }
}

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    Hash static_allocated_instance; // Prevents seg fault :S
    Hash *host_X = &static_allocated_instance;
    host_X->t = (uint32_t)time(NULL);
    size_t size = 52 * sizeof(uint8_t);
    uint8_t *dev_X;
    uint8_t *dev_result;
    uint8_t host_result[40];
    srand(0);

    parse_input(host_X);

// Test with fixed Time / Epoch 
IF_TEST(
    host_X->t = Swap4Bytes( 0x5bb16380 );
)// DEBUG  SECTION |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
IF_DEBUG(
    int device_idx, count;
    cudaDeviceProp static_instance; // Prevents seg fault :S
    cudaDeviceProp *device = &static_instance;
    
    gpuErrchk( cudaChooseDevice        (   &device_idx, device )    ); 
    gpuErrchk( cudaGetDevice	        (	&device_idx	)           );
    gpuErrchk( cudaGetDeviceCount      (	&count	    )           ); 	
    gpuErrchk( cudaGetDeviceProperties	(	device, device_idx	)   );
    cout << " -------------------- DEVICE " << device_idx << "INFO -----------------------------------" << endl;
    cout << "Name                       " << device->name << endl;
    cout << "device->totalGlobalMem     " << device->totalGlobalMem <<endl;
    cout << "device->sharedMemPerBlock  " << device->sharedMemPerBlock <<endl;
    cout << "device->regsPerBlock       " << device->regsPerBlock <<endl;
    cout << "device->warpSize           " << device->warpSize <<endl;
    cout << "device->maxThreadsPerBlock " << device->maxThreadsPerBlock <<endl;
    cout << "device->maxThreadsDim      " << device->maxThreadsDim <<endl;
    cout << "device->maxGridSize        " << device->maxGridSize <<endl;
    cout << "device->major              " << device->major <<endl;
    cout << "device->minor              " << device->minor <<endl;
    cout << "device->clockRate          " << device->clockRate <<endl;
    cout << "device->computeMode        " << device->computeMode << endl;

    printf("    Hash out = ");
    for (int i = 0; i < 52; i++)
        printf("%02x", host_X->byte[i]); 
    printf("\n\n"); 
)// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    //Allocate vectors in device memory   
    gpuErrchk(  cudaMalloc( (void**)&dev_X, 52 * sizeof(uint8_t) )      );
    gpuErrchk(  cudaMalloc( (void**)&dev_result, 40 * sizeof(uint8_t) ) );
    
    // Copy host memory to device memory 
    gpuErrchk( cudaMemcpy( dev_X, &host_X->byte, size, cudaMemcpyHostToDevice )     );


   // for ( int loop=0; loop < 20;loop++)
    {   
        // set global keep_running variable in device memery
        int dummy = 1;
        cudaMemcpyToSymbol( keep_running, &dummy, sizeof(int)     );

        // Invoke kernel from argument or from static block dim
        if (argc == 3)
        {   
            int gridDim  = get_argument( argv[2] );
            int blockDim = get_argument( argv[1] );
            kernel<<<gridDim,blockDim>>>(dev_X, dev_result);
        }
        else 
        {
            dim3 gridDim (2); // No reason to use 2d or 3d structure 
            dim3 blockDim(2);
            kernel<<<gridDim,blockDim>>>(dev_X, dev_result);
        }

        cudaPeekAtLastError() ;
        cudaDeviceSynchronize() ;
        cudaDeviceSynchronize();   
    }

    // Copy result from device to host memory
    cudaMemcpy( host_result, dev_result, 40 * sizeof(uint8_t), cudaMemcpyDeviceToHost );

    printf("%s\n", NUSNET_ID);
    printf("%lu \n", (unsigned long int)Swap4Bytes( host_X->t ) );

    uint64_t *ptr64 = (uint64_t *)host_result;
    printf("%llu\n", (unsigned long long int)Swap8Bytes( *ptr64 ) );

    for (int i=8; i < 40;i++) printf("%02x", host_result[i]);
    printf("\n");

    cudaFree(dev_X); cudaFree(dev_result);
    return 0;
}

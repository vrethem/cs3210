Compile and run: 
make 
./main 


Program can take 1D sizes of gridDim and blockDim as argument,
example shows gridDim = 32 and blockDim = 64:
./main 32 64
./main 32 64 < input.txt


Run with text file as input:
./main < input.txt








//////////////////////////////////////////////////////
Compile:
nvcc -arch sm_50 -std=c++11 --device-c main.cu hash.cu 
nvcc -arch sm_50 -std=c++11 -o main main.o hash.o 

Run:
./main

Run profiler with: 
/usr/local/cuda/bin/nvprof ./main < input.txt
or 
nvprof ./main < input.txt




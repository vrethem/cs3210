
//                             256 bit              1664 bit
//                             64 hexs              416 hexs      size_t = 416
//                              Output   ,          Input
//__device__ void sha256(uint8_t hash[32], const  * input, size_t len)
// hash[32] == 48*4, input == 16*4
#include <sstream>
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <vector>

using namespace std;


/**
 * Adds up 1,000,000 times of the block ID to
 * a variable.
 * What to observe/ponder:
 * - Any difference between shared and global memory?
 * - Does the result differ between runs?
 */



/*
__device__ __managed__ int global_counter[2];

void check_cuda_errors()
{
    cudaError_t rc;
    rc = cudaGetLastError();
    if (rc != cudaSuccess)
    {
        printf("Last CUDA error %s\n", cudaGetErrorString(rc));
    }

}

__global__ void shared_mem(int times)
{
    __shared__ int shared_counter[2];
    int i;

    // Zero out both counters
    shared_counter[threadIdx.x] = 0;

    for (i = 0; i < times; i++) {
        shared_counter[threadIdx.x] += blockIdx.x;
    }

    printf("Shared (Blk: %d, Th: %d): %d\n", blockIdx.x, threadIdx.x, shared_counter[threadIdx.x]);
}

__global__ void global_mem(int times)
{
    int i;

    // Zero out both counters
    global_counter[threadIdx.x] = 0;

    for (i = 0; i < times; i++) {
        global_counter[threadIdx.x] += blockIdx.x;
    }

    printf("Global (Blk: %d, Th: %d): %d\n", blockIdx.x, threadIdx.x, global_counter[threadIdx.x]);
}

int main(int argc, char **argv)
{
    shared_mem<<<10, 2>>>(1000000);
    cudaDeviceSynchronize();
    check_cuda_errors();

    global_mem<<<10, 2>>>(1000000);
    cudaDeviceSynchronize();
    check_cuda_errors();

    return 0;
}
*/

vector<string> parse_input(string previous_digest)
{
    for (int i=0; i < d_str.length(); i+=16) {
        string dword = "";
        for (int x=0; x < 16;x++ ) {
            dword += previous_digest[i+x];
        }
        d_v.push_back(dword);
    }

    for (int i=0; i < d_v.size(); i+=1) {
        stringstream ss;
        ss << std::hex << d_v.at(i);
        ss >> d[i];

        cout << d_v.at(i) << " == ";
        cout << d[i] << endl;
    }
}
int main()
{//3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71
    string d_str = "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71";
    string n_str = "281474976710656";

    // d can be stored as a string
    // output is prefferable stored as uint64 
    uint64_t d[4];
    uint64_t n;
    vector<string> d_v;

    
    

    {
        stringstream str(n_str);
        str >> dec >> n;
    }
    
    cout << "0x" << d_str << " == " << d[3] << "," << d[2] << "," << d[1] << "," << d[0] << endl;
    cout << "0x" << n_str << " == " << n << endl;
    
    uint64_t testd;
    string test = "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71";
    stringstream ss;
    ss << std::hex << test;
    ss >> testd;
    cout << testd << endl;
    return 0;
}




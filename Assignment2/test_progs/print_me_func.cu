////////////////////////////////////////////
// CS3210 - Assignment 2 
// Why is it so slow? 
//      - To much compution per thread in warp?
//      - To much memory per thread ?
// See definitions.h for definitions, 
//  help functios, methods and datastructure union Hash   
#include "definitions.h" 
#include "global_thread_id.h"
#include "hash.h"
#include <assert.h>

#define MIN 0 
#define MAX 0xFFFFFFFFFFFFFFFF // 2^64 -1

using namespace std;

/*union Hash {
    uint8_t byte[52];
    struct
    {
        uint32_t t;
        uint64_t prev_digest[4];
        uint64_t id;
        uint64_t nonce;
    } __attribute__((packed));
} __attribute__((packed));
typedef union Hash Hash;*/

__device__ int threadsPerBlock()  {    return blockDim.x * blockDim.y * blockDim.z;                       }
__device__ int threadNumInBlock() {   return threadIdx.y + blockDim.y * threadIdx.x;                      }
__device__ int blockNumInGrid()   {    return blockIdx.y  + gridDim.y  * blockIdx.x;                      }
__device__ int tid()              {    return blockNumInGrid() * threadsPerBlock() + threadNumInBlock();  }
__device__ int threads()          {    return threadsPerBlock() * gridDim.x * gridDim.y * gridDim.z;      }



__global__ void print_me()
{
    __shared__ uint64_t shared_n[8];
    __shared__ uint8_t shared_X[44]; 
    __shared__ int shared_keep_searching;
    printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) GridDim(%2d,%2d,%2d) BlockDim(%2d,%2d,%2d)\n", 
                blockIdx.x,blockIdx.y,blockIdx.z,
                threadIdx.x,threadIdx.y,threadIdx.z,
                gridDim.x, gridDim.y, gridDim.z,
                blockDim.x, blockDim.y, blockDim.z);
    uint8_t hash[1024];
    memset(hash, 0, 1024);
    uint8_t result[32];
    //sha256(result, hash, 52);
}
__global__ void print_me2()
{
    __shared__ uint64_t shared_n[8];
    __shared__ uint8_t shared_X[44]; 
    __shared__ int shared_keep_searching;
    printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) GridDim(%2d,%2d,%2d) BlockDim(%2d,%2d,%2d)\n", 
                blockIdx.x,blockIdx.y,blockIdx.z,
                threadIdx.x,threadIdx.y,threadIdx.z,
                gridDim.x, gridDim.y, gridDim.z,
                blockDim.x, blockDim.y, blockDim.z);
    uint8_t hash[52];
    memset(hash, 0, 52);
    uint8_t result[32];
    sha256(result, hash, 52);
}

/*
 * Has to be called with a minimum of 44 threads in X
 * --- RETURNS      [ Nonce  ,  digest  ]    1D- array             
 * uint8_t successor[ 8      +  32      ]
 *                    64     +  256  bits
 */
__global__ void try_hash_func(uint8_t *dev_X_in, uint8_t *dev_result)
{
    //////////////////////////
    // Calculate tid Based on defintions in __GLOBAL_THREAD_ID_H__
    /* SAVE ON LOCAL MEM 
    int threadsPerBlock = 
     int threadNumInBlock = 
     int blockNumInGrid   = 
     int tid = ;*/
    
    //unsigned int tid = getGlobalIdx_1D_2D();
    //   int threads() = threads()PerBlock * gridDim.x * gridDim.y * gridDim.z;

      //////////////////////////
    // Global shared memory on device
    __shared__ uint64_t shared_n[8];
    __shared__ uint8_t shared_X[44]; 
    __shared__ int shared_keep_searching;
    __shared__ uint64_t step_size;
    __shared__ uint64_t iteration;
    uint8_t result[32];
    uint32_t *ptr32 = NULL;
    uint64_t *ptr64 = NULL;
    

    if (tid() == 0)   
    {
        shared_keep_searching = 1;
        step_size = 0x4000000000000; // 2^64 / 2^10
        iteration = 0;
    }
    

   /* if ( threads() >= 44 ) {
        if (tid() < 44)   shared_X[tid()] = dev_X_in[tid()];
        if (tid() < 8 )   shared_n[tid()] = dev_X_in[44 + tid()];
    }
    else { Above don't work :S:S:S:*/ 
       /* if ( tid() == 0) {
            memcpy(shared_n, (dev_X_in+sizeof(uint8_t)*44), 8 );
            memcpy(shared_X, dev_X_in, 44);    
        }*/
    //}
    
    if (tid() == threads() -1){
        printf("%llu - Im here \n", tid());
    }
    
    __syncthreads();// wait for each thread to copy its elemenet

    //////////////////////////
    // Local Memory on Device
    //Hash instance;
    //Hash *X = &instance;
    
    //////////////////////////
    // Memcpy  to local 
    //memcpy(&X->byte, shared_X, 52);

    //////////////////////////
    // Test local memory
IF_DEBUG(
    if (tid() == 0)
    {
        printf("--------------------------------------------------------------------------------------------------------\n");
        printf("------------------------------ Test Of Input -----------------------------------------------------------\n");
        printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) GridDim(%2d,%2d,%2d) BlockDim(%2d,%2d,%2d)  tid() == %6u , MAX == %6u blks = %2u\n", 
                blockIdx.x,blockIdx.y,blockIdx.z,
                threadIdx.x,threadIdx.y,threadIdx.z,
                gridDim.x, gridDim.y, gridDim.z,
                blockDim.x, blockDim.y, blockDim.z,
                tid(), threads(), blockNumInGrid);

        uint8_t ptr[8] = {0x00, 0x00, 0x35, 0x63, 0x67, 0x3b, 0xfa, 0x11}; 
        ptr64 = (uint64_t *)ptr;
        printf("uint8_t reads [");
        for (int i = 0; i < 8; i++)
            printf("%02x", ptr[i]);
        printf("] uint64_t reads [%016llx]\n", *ptr64);
        if (*ptr == *ptr64 >> sizeof(uint8_t) * 8)
            printf("¤¤¤¤¤¤ DEVICE USES BIG ENDIAN  ¤¤¤¤¤¤ \n");
        else
            printf("¤¤¤¤¤¤ DEVICE USES LITLLE ENDIAN ¤¤¤¤¤ \n");
            
        printf("TEST HASH STRING IN  == ");
        for (int i=0; i < 52; i++) printf("%02x", dev_X_in[i]);
        printf("\n");    
        printf("SHARED  STRING       == ");
        for (int i=0; i < 52; i++) printf("%02x", shared_X[i]);
        printf("\n");
        printf("LOCAL   STRING       == ");
        for (int i=0; i < 52; i++) printf("%02x", dev_X_in[i]);
        printf("\n");
        printf("SHARED n  base10     == %llu\n", Swap8Bytes(*shared_n));
        printf("SHARED n  base16     == %llx\n", Swap8Bytes(*shared_n));
        ptr32 = (uint32_t*)dev_X_in;
        printf("EPOCH     base10     == %u\n", Swap4Bytes( *ptr32) );
        printf("EPOCH     base16     == %08x\n", Swap4Bytes( *ptr32) );
        
        //X->t = Swap4Bytes( 0x5bb16380 );
        //X->nonce = Swap8Bytes(0xe69d030000000000);
        printf("[Test] with hash X   == ", tid());
        for (int i = 0; i < 52; ++i) printf("%02x", dev_X_in[i]);
        printf("\n");        //////////////////////////
        // Calculate 'n' Work
        sha256(result, dev_X_in, 52);

        printf("SHA256 outputs       == ", tid());
        for (int i = 0; i < 32; ++i) printf("%02x", result[i]);
        printf("\n");
        
        ptr64 = (uint64_t *)result;
        printf("     Result base10   == %llu\n", Swap8Bytes( *ptr64 ) );
        printf("     Result base16   == %016llx\n", Swap8Bytes( *ptr64 ) );
        printf("--------------------------------------------------------------------------------------------------------\n");
        printf("- Execute Main Loop ----------(Big Endian)-------------(Big Endian)-------------------------------------\n");
        printf("( tid() )[  ITERATION NUM ]       NONCE      =SHA256=>    HEX RESULT    ==        DEC RESULT              \n");
        printf("(    0)[               0] 0000000000000000 =SHA256=> 911df0aab1f6e57e == 10456778525745014142           \n");
        }
)
    //////////////////////////
    // Initialize nonce                0x2A1B86986E 
    uint64_t nonce = step_size * tid(); // Time Costly calculation?? Only done once per tid()
    
    printf("Grid(%2d,%2d,%2d)Block(%2d,%2d,%2d) [%6u] nonce =%016llx\n", 
        blockIdx.x,blockIdx.y,blockIdx.z,
        threadIdx.x,threadIdx.y,threadIdx.z,
        tid(), nonce);

    
    /*
    *  While shared_keep_searching is true 
    */ 
    __syncthreads();
    while ( iteration < step_size && shared_keep_searching )
    {
        //////////////////////////
        // Memcpy nonce
        //memcpy(&(X->byte[44]), &nonce, 8);

        //////////////////////////
        // Calculate 
        sha256(result, dev_X_in, 52);

        //
        // DEBUG
        //
    IF_DEBUG(
        ptr64 = (uint64_t *)result;
        printf("(%5d)[%16llx] %016llx =SHA256=> %016llx == %18llu\n", tid(), iteration, nonce, Swap8Bytes( *ptr64 ),Swap8Bytes( *ptr64 ) );
    )
        //////////////////////////
        // Check result
        ptr64 = (uint64_t*)result;
        if ( Swap8Bytes( *ptr64 ) < *shared_n)
        {
            memcpy(dev_result, ((uint8_t*)&nonce), 8 );
            memcpy(dev_result+sizeof(uint8_t)*8, (uint8_t*)result, 32 );

            printf("<<<(%6d)>>> Result: %llu < %llu\n", iteration, Swap8Bytes( *ptr64 ), *shared_n);
            shared_keep_searching = 0;
            break;
        }

        //iteration++;
        nonce += 1;//iteration; //maybe better spread?
    }
    if (tid() == 0) 
    {
        printf("--------------------------------------------------------------------------------------------------------\n");
        iteration++;
    }
}


void check_cuda_errors()
{
    cudaError_t rc;
    rc = cudaGetLastError();
    if (rc != cudaSuccess)
    {
        printf("Last CUDA error %s\n", cudaGetErrorString(rc));
    }
}

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    cudaError_t t;
    int device_idx, count;
    cudaDeviceProp static_instance; // Prevents seg fault :S
    cudaDeviceProp *device = &static_instance;
    
    t = cudaChooseDevice        (   &device_idx, device ); 
    t = cudaGetDevice	        (	&device_idx	);
    t = cudaGetDeviceCount      (	&count	    ); 	
    t = cudaGetDeviceProperties	(	device, device_idx	);

IF_DEBUG(
    cout << " -------------------- DEVICE " << device_idx << "INFO -----------------------------------" << endl;
    cout << "Name                       " << device->name << endl;
    cout << "device->totalGlobalMem     " << device->totalGlobalMem <<endl;
    cout << "device->sharedMemPerBlock  " << device->sharedMemPerBlock <<endl;
    cout << "device->regsPerBlock       " << device->regsPerBlock <<endl;
    cout << "device->warpSize           " << device->warpSize <<endl;
    cout << "device->maxThreadsPerBlock " << device->maxThreadsPerBlock <<endl;
    cout << "device->maxThreadsDim      " << device->maxThreadsDim <<endl;
    cout << "device->maxGridSize        " << device->maxGridSize <<endl;
    cout << "device->major              " << device->major <<endl;
    cout << "device->minor              " << device->minor <<endl;
    cout << "device->clockRate          " << device->clockRate <<endl;
    cout << "device->computeMode        " << device->computeMode << endl;
)

    Hash static_allocated_instance; // Prevents seg fault :S
    Hash *host_X = &static_allocated_instance;
    host_X->t = (uint32_t)time(NULL);
    size_t size = 52 * sizeof(uint8_t);
    uint8_t *dev_X;
    uint8_t *dev_result;
    uint8_t host_result[40];
    cudaError_t rc;
    srand(0);


    parse_input(host_X);

     IF_DEBUG(printf("    Hash out = ");
     for (int i = 0; i < 52; i++)
         printf("%02x", host_X->byte[i]); 
     printf("\n"); )

    /*
     * Allocate vectors in device memory
    */    
    cudaMalloc( (void**)&dev_X, 52 * sizeof(uint8_t) );
    cudaMalloc( (void**)&dev_result, 40 * sizeof(uint8_t) );
    
    /**
     * Copy host memory to device memory 
     */
    rc = cudaMemcpy( dev_X, &host_X->byte, size, cudaMemcpyHostToDevice );
    if (rc != cudaSuccess)
        printf("rc. Reason: %s\n", cudaGetErrorString(rc));

    /*
     * Invoke kernel
     */ 
    //dim3 gridDim(1, 1);
    
    dim3 gridDim(1);
    dim3 blockDim(32, 32);
    print_me<<<gridDim,blockDim>>>();
    //print_me<<<gridDim,blockDim>>>(dev_X, dev_result);
    
    cudaDeviceSynchronize();
    check_cuda_errors();
    cout << " print 1 \n";
    print_me2<<<gridDim,blockDim>>>();
    cudaDeviceSynchronize();
    check_cuda_errors();
    

    // Copy result from device memory to host memory
    cudaMemcpy( host_result, dev_result, 40 * sizeof(uint8_t), cudaMemcpyDeviceToHost );

    printf("E1234567\n");
    printf("%lx \n", host_X->t);
    printf("%lx \n", host_X->nonce);
    for (int i=0; i < 8; i++) printf("%02x", host_result[i]);
    printf("\n");
    for (int i=8; i < 40;i++) printf("%02x", host_result[i]);


    cudaFree(dev_X); cudaFree(dev_result);
    return 0;
}
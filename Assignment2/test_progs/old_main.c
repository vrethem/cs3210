//
//Global Memory (Symbol)
//
/*
// * Global Memory (Symbol)
// * Demonstrates:
// * - Communication between host and device
// * - Method in which host accesses global memory
// */
#include <iostream> // cout, cin
#include <stdint.h> // For uint8_t
#include <stdio.h>  // Scanf , printf
#include <string.h> //
#include <ctime>    // For epoch
#include <sstream>  // for int to hexString conversion
#include <stdlib.h>
#include <string> // use to_string()
#include "hash.h"

using namespace std;

struct Digest
{
    uint64_t nonce;
    uint64_t id;
    uint64_t[4] prev_digest;
    uint32_t t;
};

//////////////////////////
// Global memory on device
__managed__ uint8_t global_result[32]; // 32 * 4 bit == 128

__device__ uint8_t dev_prev_digest[32]; // 64 * 4 bit == 256
__device__ uint8_t dev_epoch[4];        //  8 * 4 bit == 32
__device__ uint8_t dev_id[8];           // 16 * 4 bit == 64
__device__ uint8_t dev_n[8];            //  8 * 8 bit == 64

void check_cuda_errors()
{
    cudaError_t rc;
    rc = cudaGetLastError();
    if (rc != cudaSuccess)
    {
        printf("Last CUDA error %s\n", cudaGetErrorString(rc));
    }
}

__global__ void validator()
{
    printf("(%2d,%2d,%2d) %s\n", threadIdx.x, threadIdx.y, threadIdx.z, dev_prev_digest);
    printf("(%2d,%2d,%2d) %s\n", threadIdx.x, threadIdx.y, threadIdx.z, dev_epoch);
    printf("(%2d,%2d,%2d) %s\n", threadIdx.x, threadIdx.y, threadIdx.z, dev_id);
    printf("(%2d,%2d,%2d) %s\n", threadIdx.x, threadIdx.y, threadIdx.z, dev_n);
}

__global__ void try_hash_func()
{
    uint8_t result[32];
    char nonce[16] = "123456789abcdef";
    char digest[104];

    /*  strcpy (digest, dev_epoch);
    strcat (digest, dev_prev_digest);
    strcat (digest, dev_id);
    strcat (digest, nonce);
    
*/
    //const uint8_t *digest_uint = &digest[0];
    sha256(result, reinterpret_cast<const uint8_t *>(&digest[0]), 416 / 4);
}

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    char t_hex[8];
    char d_str[64];
    char id_str[8];
    char id_hex[16];
    char n_str[16];
    uint8_t n[8];
    uint8_t d[32];
    uint8_t id[8];
    uint32_t t = (uint32_t)time(NULL);

    cudaError_t rc, rc1, rc2, rc3;
    srand(0);

    /**
     * PARSE  INPUT 
     */
    strcpy(d_str, "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71");
    {
        stringstream ss;
        ss << dec << d_str;
        ss >> d;
        cout << "Test d: " << n << endl;
    }
    strcpy(n_str, "281474976710656");
    {
        stringstream ss;
        ss << n_str;
        ss >> n;
        cout << "Test n: " << n << endl;
    }
    // Get current time epoch
    {
        stringstream ss;
        ss << hex << t;
        ss >> t_hex;
        cout << "Test t: " << t_hex << endl;
    }

    // Convert text to ASCII to hex
    strcpy(id_str, "E1234567");
    {
        string hex_tmp;
        for (int i = 0; i < 8; i += 1)
        {
            // Convert to ASCII
            int num = (int)id_str[i];
            string tmp = to_string(num / 16) + to_string(num % 16);
            // Convert to hex
            id_hex[(i * 2)] = tmp[0];
            id_hex[(i * 2) + 1] = tmp[1];
        }
        //id_hex[16] = '\0';
    }
    {
        stringstream ss;
        ss << dec << id_str;
        ss >> id;
        cout << "Test id: " << id << endl;
    }

    /**
     * Copy host memory to device memory 
     */
    rc = cudaMemcpyToSymbol(dev_prev_digest, &d_str, sizeof(char) * 64); // 32 * 8 bit
    rc1 = cudaMemcpyToSymbol(dev_epoch, &t_hex, sizeof(t_hex));          // 4  * 8 bit
    rc2 = cudaMemcpyToSymbol(dev_id, &id_hex, sizeof(char) * 16);        // 8  * 8 bit
    rc3 = cudaMemcpyToSymbol(dev_n, &n, sizeof(n));                      // 8  * 8 bit

    if (rc != cudaSuccess || rc1 != cudaSuccess || rc2 != cudaSuccess || rc3 != cudaSuccess)
    {
        printf("CudaMemcpy error\n");
        printf("rc. Reason: %s\n", cudaGetErrorString(rc));
        printf("rc1. Reason: %s\n", cudaGetErrorString(rc1));
        printf("rc2. Reason: %s\n", cudaGetErrorString(rc2));
        printf("rc3. Reason: %s\n", cudaGetErrorString(rc3));
    }

    /*
     * Copy host memory to device memory 
     */
    //validator<<<2, 2>>>();
    try_hash_func<<<2, 2>>>();

    cudaDeviceSynchronize();
    check_cuda_errors();

    // Retrieve data from global memory variable
    //rc = cudaMemcpyFromSymbol(&host_result, result, sizeof(start));

    // Has global memory
    printf("Incrementor results:\n");
    for (int i = 0; i < 4; i++)
    {
        printf("result[%d] = %d\n", i, global_result[i]);
    }
    return 0;
}
#include <iostream> // cout, cin
#include <stdint.h> // For uint8_t
#include <stdio.h>  // Scanf , printf
#include <string.h> // 
#include <ctime>    // For epoch 
#include <sstream>  // for int to hexString conversion
#include <stdlib.h>
#include <string>   // use to_string()
#include <bitset>
#include <iomanip>

#include "hash.h"

using namespace std;

union Digest {
    uint8_t byte[52];
    struct
    {
        uint32_t t[1];
        uint64_t prev_digest[4];
        uint64_t id[1];
        uint64_t nonce[1];
    };
};

#define Swap8Bytes(val) \
 ( (((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
   (((val) >> 24) & 0x0000000000FF0000) | (((val) >>  8) & 0x00000000FF000000) | \
   (((val) <<  8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) | \
   (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000) )


// Device code
__global__ void validator(uint8_t *dev_X, uint8_t *dev_result)
{

    union Digest *X = (union Digest*)dev_X;
    
    printf("(%2d,%2d,%2d) %llx\n", threadIdx.x, threadIdx.y, threadIdx.z, X->t);
    printf("(%2d,%2d,%2d) %llx", threadIdx.x, threadIdx.y, threadIdx.z, X->prev_digest[0]);
    printf("%llx", threadIdx.x, threadIdx.y, threadIdx.z, X->prev_digest[1]);
    printf("%llx", threadIdx.x, threadIdx.y, threadIdx.z, X->prev_digest[2]);
    printf("%llx\n", threadIdx.x, threadIdx.y, threadIdx.z, X->prev_digest[3]);
    printf("(%2d,%2d,%2d) %llx\n", threadIdx.x, threadIdx.y, threadIdx.z, X->id);
    printf("(%2d,%2d,%2d) %llx\n", threadIdx.x, threadIdx.y, threadIdx.z, X->nonce);


    printf("Hash in = ");
    for (int i = 0; i < 52; i++)
        printf("%02x", X->byte[i]); 
    printf("\n");

    uint8_t result_long[32];
    sha256(result_long, dev_X, 52);
    
    printf("result_long = ");
    for (int i = 0; i < 32; i++)
        printf("%02x", result_long[i]);  
    printf("\n");
    
    uint64_t *result = (uint64_t*)result_long;
    uint64_t limit = 0x1000000000000;
    if ( Swap8Bytes(*result) < limit)
    {
        for (int i=44; i < 52;i++) {
            dev_result[i] = dev_X[i];
        }
        printf("Result: %llu ", Swap8Bytes(*result));
        printf(" <   : %llu\n", limit);
    }
    
}

// Host code
int main()
{
    // Allocate vectors in device memory
    size_t size = 52 * sizeof(uint8_t);
    uint8_t *dev_X;
    uint8_t *dev_result;
    uint8_t host_result[8];
    uint8_t host_X[52] = {  
        //  5bb16380
            0x5b,   0xb1,   0x63,   0x80,      
        //  3210327c68bb9409c4aa5806a4c018e2
            0x32,   0x10,   0x32,   0x7c,   0x68,   0xbb,   0x94,   0x09,   0xc4,   0xaa,   0x58,   0x06,   0xa4,   0xc0,   0x18,   0xe2, 
        //  6dcd2ca599a5cbccfaf09c886f701b71
            0x6d,   0xcd,   0x2c,   0xa5,   0x99,   0xa5,   0xcb,   0xcc,   0xfa,   0xf0,   0x9c,   0x88,   0x6f,   0x70,   0x1b,   0x71,
        //  4531323334353637
            0x45,   0x31,   0x32,   0x33,   0x34,   0x35,   0x36,   0x37,
        //  e69d030000000000
            0xe6,   0x9d,   0x03,   0x00,   0x00,   0x00,   0x00,   0x00
    };

    

    cudaMalloc( (void**)&dev_X, 52 * sizeof(uint8_t) );
    cudaMalloc( (void**)&dev_result, 8 * sizeof(uint8_t) );
    
    // Copy vectors from host memory to device memory
    // h_A and h_B are input vectors stored in host memory
    cudaMemcpy( dev_X, host_X, size, cudaMemcpyHostToDevice );
    
    
    // Invoke kernel
    validator<<<1, 1>>>(dev_X, dev_result);

    // Copy result from device memory to host memory
    cudaMemcpy( host_result, dev_result, 8 * sizeof(uint8_t), cudaMemcpyDeviceToHost );

    cudaFree(dev_X); cudaFree(dev_result);
}
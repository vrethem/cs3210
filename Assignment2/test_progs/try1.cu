/**
 * Global Memory (Symbol)
 * Demonstrates:
 * - Communication between host and device
 * - Method in which host accesses global memory
 */
 #include <sstream>
 #include <iostream>
 #include <stdint.h>
 #include <stdio.h>
 #include <string.h>

 //////////////////////////
 // Global memory on device
 __managed__ uint8_t result[32];
 __device__  char previous_digest[64];
 __device__  uint8_t epoch[4];
 __device__  char id[8];


 void check_cuda_errors()
{
    cudaError_t rc;
    rc = cudaGetLastError();
    if (rc != cudaSuccess)
    {
        printf("Last CUDA error %s\n", cudaGetErrorString(rc));
    }
}




__global__ void incrementor()
{
    printf("%s\n", previous_digest);
    result[threadIdx.x]++;
}

int main(int argc, char **argv)
{
    char d_str[64];
    strcpy(d_str, "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71");
    char n_str[16];
    strcpy(n_str, "281474976710656");

    cudaError_t rc;

    // Seed our RNG
    srand(0);

    /**
     * Copy hash to device 
     */
    rc = cudaMemcpyToSymbol(previous_digest, &d_str, sizeof(d_str));

    if (rc != cudaSuccess)
    {
        printf("Could not copy to device. Reason: %s\n", cudaGetErrorString(rc));
    }

    incrementor<<<1, 64>>>();
    
    cudaDeviceSynchronize();
    check_cuda_errors();

    // Retrieve data from global memory variable
    //rc = cudaMemcpyFromSymbol(&host_result, result, sizeof(start));

    // Has global memory
    printf("Incrementor results:\n");
    for (int i = 0; i < 64; i++) {
        printf("result[%d] = %d\n", i, result[i]);
    }
    return 0;
}
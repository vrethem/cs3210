//
//Global Memory (Symbol)
//
/*
// * Global Memory (Symbol)
// * Demonstrates:
// * - Communication between host and device
// * - Method in which host accesses global memory
// */
// HEX:  3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71 
// DEC:  22644260423580143382539648509814936654585960649098714602196347919214530534257
// BIN:  0011001000010000001100100111110001101000101110111001010000001001110001001010101001011000000001101010010011000000000110001110001001101101110011010010110010100101100110011010010111001011110011001111101011110000100111001000100001101111011100000001101101110001

#include <iostream> // cout, cin
#include <stdint.h> // For uint8_t
#include <stdio.h>  // Scanf , printf
#include <string.h> // 
#include <ctime>    // For epoch 
#include <sstream>  // for int to hexString conversion
#include <stdlib.h>
#include <string>   // use to_string()
#include <bitset>
#include <iomanip>

#include "hash.h"

using namespace std;

 __device__  char dev_d_hex[8];         //  4 * 8 bit == 32

 void hexStrToUint8(string hex, uint8_t *out);
 __global__ void uintToHexStr(uint8_t *in); 


__global__ void validator()
{
    //__constant__ uint8_t n();
    printf("(%2d,%2d,%2d) %s\n", threadIdx.x, threadIdx.y, threadIdx.z, *dev_d_hex);
}

void check_cuda_errors()
{
    cudaError_t rc;
    rc = cudaGetLastError();
    if (rc != cudaSuccess)
    {
        printf("Last CUDA error %s\n", cudaGetErrorString(rc));
    }
}

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    uint32_t t = (uint32_t)time(NULL);
    cudaError_t rc;
    srand(0);

    /**
     * PARSE  INPUT 
     */  
     const char *d_hex = {"3210327E"};
 
 
     rc = cudaMemcpyToSymbol(dev_d_hex, d_hex,     sizeof(char)*8, cudaMemcpyKind::cudaMemcpyHostToDevice);   // 4  * 8 bit

    if (rc != cudaSuccess)
    {
        printf("CudaMemcpy error\n");
        printf("rc. Reason: %s\n", cudaGetErrorString(rc));
    }

    /*
     * Copy host memory to device memory 
     */
    
    validator<<<1, 1>>>();
    cudaDeviceSynchronize();
    check_cuda_errors();
    
    return 0;
}
////////////////////////////////////////////
// CS3210 - Assignment 2 
// Anders Vrethem, Ruslan
//
// See definitions.h for helper macros and 
//  union Hash datastructure  
//
// IF_DEBUG( .. ) is a wrapper for debugging
//                enable code by adding -DDEBUG flag at compile time
// gpuErrchk( .. ) is a wrapper for cudaError's 
#define IS_BIG_ENDIAN 0
#define NUSNET_ID "E1234567"

#include "definitions.h" 
#include "hash.h"

#define ITERATIONS 0x0c15c
using namespace std;

/*union Hash {
    uint8_t byte[52];
    struct
    {
        uint32_t t;
        uint64_t prev_digest[4];
        uint64_t id;
        uint64_t nonce_Little_End;
    } __attribute__((packed));
} __attribute__((packed));
typedef union Hash Hash;*/

__device__ int threadsPerBlock()  {    return blockDim.x * blockDim.y * blockDim.z;                                     }
__device__ int threadsPerRow()    {    return blockDim.x * blockDim.y;                                                  }
__device__ int threadNumInBlock() {    return threadIdx.y + blockDim.y * threadIdx.x + threadIdx.z * threadsPerRow();   }
__device__ int blocksPerRow()     {    return gridDim.x * gridDim.y;                                                    }
__device__ int blocksPerGrid()    {    return gridDim.x * gridDim.y * gridDim.z;                                        }
__device__ int blockNumInGrid()   {    return blockIdx.y  + gridDim.y  * blockIdx.x + blockIdx.z * blocksPerRow();      }
__device__ int tid()              {    return threadNumInBlock();                                                       }
__device__ int global_tid()       {    return blockNumInGrid() * threadsPerBlock() + threadNumInBlock();                }
__device__ int threads()          {    return threadsPerBlock() * blocksPerGrid();                                      }

__device__ int keep_running = 1;


__global__ void shared(uint8_t *dev_X_in, uint8_t *dev_result)
{    
    //////////////////////////
    // Global shared memory on device
    __shared__ int shared_keep_running; // CANT BE SHARED MUST BE GLOBAL!!!
    uint64_t local_n;

    //////////////////////////
    // Local Memory on Device
    Hash instance;
    Hash *X = &instance;  
    uint8_t result[32];
    uint64_t *ptr64 = NULL;
    unsigned long long int iteration = 0; 

    Hash *ptr = (Hash *)dev_X_in;
    memcpy(&local_n, &ptr->nonce_Little_End, 8);

    if (tid() == 0)   
    {
        shared_keep_running  = 1;
    }    
      
    memcpy(&X->byte, dev_X_in, 44);

    {
        uint64_t step_size = (UINT64_MAX / ((uint64_t)threads()));      // Let each thread start with uniform distance to the next thread
        printf("step_size = %llx\n", step_size);
        X->nonce_Little_End = step_size * global_tid(); 
    }

    __syncthreads();
    int local_keep_running = 1;
    ptr64 = (uint64_t *)result;
    do
    {
	    X->nonce_Little_End++;

        sha256(result, X->byte, 52);
        iteration++; 
        
        if (tid() == 0 ) 
            shared_keep_running = keep_running; // minimize access time for global variable

        local_keep_running = shared_keep_running;

    } while ( iteration < ITERATIONS );     
}


__global__ void global(uint8_t *dev_X_in, uint8_t *dev_result)
{    
    //////////////////////////
    // Global shared memory on device
    __shared__ int shared_keep_running; // CANT BE SHARED MUST BE GLOBAL!!!
    uint64_t local_n;

    //////////////////////////
    // Local Memory on Device
    Hash instance;
    Hash *X = &instance;  
    uint8_t result[32];
    uint64_t *ptr64 = NULL;
    unsigned long long int iteration = 0; 

    Hash *ptr = (Hash *)dev_X_in;
    memcpy(&local_n, &ptr->nonce_Little_End, 8);

    if (tid() == 0)   
    {
        shared_keep_running  = 1;
    }    
      
    memcpy(&X->byte, dev_X_in, 44);

    {
        uint64_t step_size = (UINT64_MAX / ((uint64_t)threads()));      // Let each thread start with uniform distance to the next thread
        printf("step_size = %llx\n", step_size);
        X->nonce_Little_End = step_size * global_tid(); 
    }

    __syncthreads();
    int local_keep_running = 1;
    ptr64 = (uint64_t *)result;
    do
    {
	    X->nonce_Little_End++;

        sha256(result, X->byte, 52);
        iteration++; 
        
        local_keep_running = keep_running;

    } while ( iteration < ITERATIONS );  
}

__global__ void none(uint8_t *dev_X_in, uint8_t *dev_result)
{    
    //////////////////////////
    // Global shared memory on device
    __shared__ int shared_keep_running; // CANT BE SHARED MUST BE GLOBAL!!!
    uint64_t local_n;

    //////////////////////////
    // Local Memory on Device
    Hash instance;
    Hash *X = &instance;  
    uint8_t result[32];
    uint64_t *ptr64 = NULL;
    unsigned long long int iteration = 0; 

    Hash *ptr = (Hash *)dev_X_in;
    memcpy(&local_n, &ptr->nonce_Little_End, 8);

    if (tid() == 0)   
    {
        shared_keep_running  = 1;
    }    
      
    memcpy(&X->byte, dev_X_in, 44);

    {
        uint64_t step_size = (UINT64_MAX / ((uint64_t)threads()));      // Let each thread start with uniform distance to the next thread
        printf("step_size = %llx\n", step_size);
        X->nonce_Little_End = step_size * global_tid(); 
    }

    __syncthreads();
    int local_keep_running = 1;
    ptr64 = (uint64_t *)result;
    do
    {
	    X->nonce_Little_End++;

        sha256(result, X->byte, 52);
        iteration++; 

    } while ( iteration < ITERATIONS );     
}

int get_argument(char * argument)
{
    std::istringstream iss( argument );
    int val;
    if (iss >> val)
    { // Conversion successful
        return val;
    }
    else 
    {
        cerr << "Invalid conversion of argument" << endl;
        exit(-1);
    }
}

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    Hash static_allocated_instance; // Prevents seg fault :S
    Hash *host_X = &static_allocated_instance;
    host_X->t = (uint32_t)time(NULL);
    size_t size = 52 * sizeof(uint8_t);
    uint8_t *dev_X;
    uint8_t *dev_result;
    uint8_t host_result[40];
    srand(0);

    parse_input(host_X);

    //Allocate vectors in device memory   
    gpuErrchk(  cudaMalloc( (void**)&dev_X, 52 * sizeof(uint8_t) )      );
    gpuErrchk(  cudaMalloc( (void**)&dev_result, 40 * sizeof(uint8_t) ) );
    
    
    // Copy host memory to device memory 
    gpuErrchk( cudaMemcpy( dev_X, &host_X->byte, size, cudaMemcpyHostToDevice )     );

    if (argc == 3)
    {   
        int gridDim  = get_argument( argv[2] );
        int blockDim = get_argument( argv[1] );
        shared<<<gridDim,blockDim>>>(dev_X, dev_result);
        /*gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaDeviceSynchronize();  
        global<<<gridDim,blockDim>>>(dev_X, dev_result);
        gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaDeviceSynchronize();  
        none<<<gridDim,blockDim>>>(dev_X, dev_result);*/
    }
    else 
    {
        dim3 gridDim (2); // No reason to use 2d or 3d structure 
        dim3 blockDim(2);
        shared<<<gridDim,blockDim>>>(dev_X, dev_result);
        /*gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaDeviceSynchronize();  
        global<<<gridDim,blockDim>>>(dev_X, dev_result);
        gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaDeviceSynchronize();  
        none<<<gridDim,blockDim>>>(dev_X, dev_result);/*
    }

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaDeviceSynchronize();   


    // Copy result from device memory to host memory
    gpuErrchk(  cudaMemcpy( host_result, dev_result, 40 * sizeof(uint8_t), cudaMemcpyDeviceToHost ) );

    printf("%s\n", NUSNET_ID);
    printf("%lu \n", (unsigned long int)Swap4Bytes( host_X->t ) );

    uint64_t *ptr64 = (uint64_t *)host_result;
    printf("%llu\n", (unsigned long long int)Swap8Bytes( *ptr64 ) );

    for (int i=8; i < 40;i++) printf("%02x", host_result[i]);
    printf("\n");

    cudaFree(dev_X); cudaFree(dev_result);
    return 0;
}


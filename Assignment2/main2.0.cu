////////////////////////////////////////////
// CS3210 - Assignment 2 
// Anders Vrethem, Ruslan
//
// See struct.h for union Hash datastructure 
//     definitions.h for helper functions 
//
// IF_DEBUG( .. ) is a wrapper for debugging
//                enable code by adding -DDEBUG flag at compile time
// gpuErrchk( .. ) is a wrapper for cudaError's 
#define IS_BIG_ENDIAN 0
#define NUSNET_ID "E1234567"

#include "definitions.h" 
#include "struct.h"
#include "kernel.h"

#define gpuErrchk(ans)                        \
    {                                         \
        gpuAssert((ans), __FILE__, __LINE__); \
    }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort)
            exit(code);
    }
}

using namespace std;

int get_argument(char * argument);

/*
 * MAIN FUNCTION
 */
int main(int argc, char **argv)
{
    Hash static_allocated_instance; // Prevents seg fault :S
    Hash *host_X = &static_allocated_instance;
    host_X->t = (uint32_t)time(NULL);
    size_t size = 52 * sizeof(uint8_t);
    uint8_t *dev_X;
    uint8_t *dev_result;
    uint8_t host_result[40];
    srand(0);

    parse_input(host_X);

// Test with fixed Time / Epoch 
IF_TEST(
    host_X->t = Swap4Bytes( 0x5bb16380 );
)// DEBUG  SECTION |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
IF_DEBUG(
    int device_idx, count;
    cudaDeviceProp static_instance; // Prevents seg fault :S
    cudaDeviceProp *device = &static_instance;
    
    gpuErrchk( cudaChooseDevice        (   &device_idx, device )    ); 
    gpuErrchk( cudaGetDevice	        (	&device_idx	)           );
    gpuErrchk( cudaGetDeviceCount      (	&count	    )           ); 	
    gpuErrchk( cudaGetDeviceProperties	(	device, device_idx	)   );
    cout << " -------------------- DEVICE " << device_idx << "INFO -----------------------------------" << endl;
    cout << "Name                       " << device->name << endl;
    cout << "device->totalGlobalMem     " << device->totalGlobalMem <<endl;
    cout << "device->sharedMemPerBlock  " << device->sharedMemPerBlock <<endl;
    cout << "device->regsPerBlock       " << device->regsPerBlock <<endl;
    cout << "device->warpSize           " << device->warpSize <<endl;
    cout << "device->maxThreadsPerBlock " << device->maxThreadsPerBlock <<endl;
    cout << "device->maxThreadsDim      " << device->maxThreadsDim <<endl;
    cout << "device->maxGridSize        " << device->maxGridSize <<endl;
    cout << "device->major              " << device->major <<endl;
    cout << "device->minor              " << device->minor <<endl;
    cout << "device->clockRate          " << device->clockRate <<endl;
    cout << "device->computeMode        " << device->computeMode << endl;

    printf("    Hash out = ");
    for (int i = 0; i < 52; i++)
        printf("%02x", host_X->byte[i]); 
    printf("\n\n"); 
)// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    //Allocate vectors in device memory   
    gpuErrchk(  cudaMalloc( (void**)&dev_X, 52 * sizeof(uint8_t) )      );
    gpuErrchk(  cudaMalloc( (void**)&dev_result, 40 * sizeof(uint8_t) ) );
    
    // Copy host memory to device memory 
    gpuErrchk( cudaMemcpy( dev_X, &host_X->byte, size, cudaMemcpyHostToDevice )     );


    for ( int loop=0; loop < 20;loop++)
    {   
        // set global keep_running variable in device memery
        int dummy = 1;
        cudaMemcpyToSymbol( keep_running, &dummy, sizeof(int)     );

        // Invoke kernel from argument or from static block dim
        if (argc == 3)
        {   
            int gridDim  = get_argument( argv[2] );
            int blockDim = get_argument( argv[1] );
            kernel<<<gridDim,blockDim>>>(dev_X, dev_result);
        }
        else 
        {
            dim3 gridDim (2); // No reason to use 2d or 3d structure 
            dim3 blockDim(2);
            kernel<<<gridDim,blockDim>>>(dev_X, dev_result);
        }

        cudaPeekAtLastError() ;
        cudaDeviceSynchronize() ;
        cudaDeviceSynchronize();   
    }

    // Copy result from device to host memory
    cudaMemcpy( host_result, dev_result, 40 * sizeof(uint8_t), cudaMemcpyDeviceToHost );

    printf("%s\n", NUSNET_ID);
    printf("%lu \n", (unsigned long int)Swap4Bytes( host_X->t ) );

    uint64_t *ptr64 = (uint64_t *)host_result;
    printf("%llu\n", (unsigned long long int)Swap8Bytes( *ptr64 ) );

    for (int i=8; i < 40;i++) printf("%02x", host_result[i]);
    printf("\n");

    cudaFree(dev_X); cudaFree(dev_result);
    return 0;
}




int get_argument(char * argument)
{
    std::istringstream iss( argument );
    int val;
    if (iss >> val)
    { // Conversion successful
        return val;
    }
    else 
    {
        cerr << "Invalid conversion of argument" << endl;
        exit(-1);
    }
}
#!/bin/bash
echo "Bash version ${BASH_VERSION}..."

make compile

LIMIT=128  # Upper limit
dridDim=1

while ((dridDim < LIMIT));
do
    dridDim=$(( 2*dridDim )) # Could maybe use step variable to step logarithmticly
    
    for N in {1..10..1}
    do
        /usr/local/cuda/bin/nvprof ./main $dridDim 1 < input.txt | grep -v "kernel"
        echo ""
    done
done

blockDim=1

while ((blockDim < LIMIT));
do
    blockDim=$(( 2*blockDim )) # Could maybe use step variable to step logarithmticly
    
    for N in {1..10..1}
    do
        /usr/local/cuda/bin/nvprof ./main $blockDim 1 < input.txt | grep -v "kernel"
        echo ""
    done
done
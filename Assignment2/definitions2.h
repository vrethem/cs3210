#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__
#include <iostream> // cout, cin
#include <stdint.h> // For uint8_t
#include <string.h> //
#include <ctime>    // For epoch
#include <sstream>  // for int to hexString conversion
#include <stdlib.h>
#include <string> // use to_string()
#include <bitset> // Debug with bitset
#include <iomanip>
#include <cmath>
#include <assert.h>
#include "struct.h"

//#define DEBUG // can be defined at compile-time by -DDEBUG flag
#ifdef DEBUG
#define IF_DEBUG(...) __VA_ARGS__
#else
#define IF_DEBUG(...)
#endif

#ifdef TEST
#define IF_TEST(...) __VA_ARGS__
#else
#define IF_TEST(...)
#endif

/*
 * Returns swapped value, does not change bytes in memory
 */
#if (IS_BIG_ENDIAN == 0)
#define Swap2Bytes(val) \
    ((((val) >> 8) & 0x00FF) | (((val) << 8) & 0xFF00))
#define Swap4Bytes(val)                                           \
    ((((val) >> 24) & 0x000000FF) | (((val) >> 8) & 0x0000FF00) | \
     (((val) << 8) & 0x00FF0000) | (((val) << 24) & 0xFF000000))
#define Swap8Bytes(val)                                                            \
    ((((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
     (((val) >> 24) & 0x0000000000FF0000) | (((val) >> 8) & 0x00000000FF000000) |  \
     (((val) << 8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) |  \
     (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000))
#else
#define Swap2Bytes(val) \
    (val)
#define Swap4Bytes(val) \
    (val)
#define Swap8Bytes(val) \
    (val)
#endif

#ifndef NUSNET_ID
#define NUSNET_ID "E1234567"
#endif

/*
 * Check if Big or Little endian???
 */
static void check_endianess()
{
    uint8_t ptr[8] = {0x00, 0x00, 0x35, 0x63, 0x67, 0x3b, 0xfa, 0x11};
    uint64_t *ptr64 = (uint64_t *)ptr;
    printf("uint8_t reads [");
    for (int i = 0; i < 8; i++)
        printf("%02x", ptr[i]);
    printf("] uint64_t reads [%016llx]\n", *(unsigned long long int *)ptr64);
    if (*ptr == *ptr64 >> sizeof(uint8_t) * 8)
        printf("¤¤¤¤¤¤ HOST USES BIG ENDIAN  ¤¤¤¤¤¤ \n");
    else
        printf("¤¤¤¤¤¤ HOST USES LITLLE ENDIAN ¤¤¤¤¤ \n");
}

/*
 * Converts a hex string to uint8_t
 */
static void hexStrToUint8(std::string hex, uint8_t *out)
{
    for (int i = 0; i < hex.length(); i += 2)
    {
        int val = std::stoi(hex.substr(i, 2).c_str(), 0, 16);
        uint8_t u_val = 0;
        u_val |= val;

        out[i / 2] = u_val;
        IF_DEBUG(std::cout << std::bitset<8>((unsigned int)u_val);)
    }
    IF_DEBUG(std::cout << std::endl;)
}

/**
 * * PARSE  INPUT 
 */
static void parse_input(Hash *host_X)
{
    IF_DEBUG(check_endianess();)
    std::string t_hex;
    std::string d_hex;
    std::string id_ascii, id_hex;
    std::string n_str, n_hex;
    unsigned long long int n_ull;

    //////////////////////////
    // Hexstring to uint8_t
    std::getline(std::cin, d_hex);

    //d_hex = "3210327c68bb9409c4aa5806a4c018e26dcd2ca599a5cbccfaf09c886f701b71";
    hexStrToUint8(d_hex, (uint8_t *)&host_X->prev_digest);

    //////////////////////////
    // Convert decimal string to uint8_t
    std::getline(std::cin, n_str);

    //n_str = "281474976710656";
    { // Read in string as a decimal number
        std::stringstream ss;
        ss << n_str;
        ss >> n_ull;
        IF_DEBUG(std::cout << n_ull << std::endl;)
    }
    { // Convert deciaml into hex string
        std::stringstream ss;
        ss << std::setfill('0') << std::setw(sizeof(host_X->nonce_Little_End) * 2) << std::hex << n_ull;
        n_hex = std::string(ss.str());
        IF_DEBUG(std::cout << n_hex << std::endl;)
    }
    hexStrToUint8(n_hex, (uint8_t *)&host_X->nonce_Little_End);

    //////////////////////////
    // Convert text to ASCII hex
    id_ascii = NUSNET_ID;
    {
        for (int i = 0; i < 8; i += 1)
        { // Convert to ASCII
            int num = (int)id_ascii[i];
            std::string tmp = std::to_string(num / 16) + std::to_string(num % 16);
            // Convert to hex
            id_hex += "  ";
            id_hex[(i * 2)] = tmp[0];
            id_hex[(i * 2) + 1] = tmp[1];
        }
    }
    hexStrToUint8(id_hex, (uint8_t *)&host_X->id);
}

#endif